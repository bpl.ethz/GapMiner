\chapter*{Part 3: Supplementary Computational Information}
\section*{Pipeline overview}
The method for predicting taggable sites in a protein is summarized as follows (see Supplementary Figure~\ref{fig:flow} for a graphical overview). Given a query amino acid sequence, sequence annotations are retrieved.
Secondary structure and relative surface areas for each residue are then predicted from the sequence and used to construct structural feature functions, respectively.

A homology search is performed to find the query's protein cluster and template HMM, and the sequences in this protein cluster are then filtered and aligned to construct a multiple sequence alignment (MSA), which is then used to train a novel profile hidden Markov model, called an insert length resolving profile hidden Markov model (ilrp-HMM), of the profile.

Length variability and sequence variability are computed for each residue from the ilrp-HMM transition and emission probabilities, respectively.
These combine with the structural scoring functions to define a feature function 
which maps each site to a 4\hyp{}dimensional feature vector. The concatenation of all of the sites' feature vectors forms a feature matrix for the query.

Finally, this feature matrix is given as input to a binary classifier trained on feature matrices and taggability labels derived from literature data. The final taggability score $\operatorname{T}$ is then computed as the decision function of the classifier.

\begin{figure}
\resizebox{!}{0.65\textwidth}{
\input{Figures/pipeline.tex}
}
\caption{\textbf{Pipeline overview}}
\label{fig:flow}
\end{figure}

\section*{Definitions}
We start by precisely defining the objects that will be used to develop the methods.

\begin{definition}
 A \emph{protein} $p$ is a string defined on the amino acid alphabet $\mathcal A'=\mathcal A\cup\{\texttt{"."},\texttt{"-"}\}$, which includes the twenty standard amino acids (represented by capital letters) and the gap characters \texttt{"."} (insertion) and \texttt{"-"} (deletion). A \emph{protein cluster} $\mathcal P$ is a cluster of proteins defined by sequences that are at least $20\%$ identical \autocite{Remmert2012}. The \emph{query protein} $p_0$ is the class representative of $\mathcal P$. The \emph{residue set} of $p_0$, denoted $\mathcal R=\{1,\ldots,\ell\}$ (where residue $1$ refers to the initial methionine), is the index set for all proteins in $\mathcal P$. The \emph{(tag insertion) sites} of $p_0$ are $\mathcal R'=\{0,1,\ldots,\ell\}$.
\end{definition}
Since insertions occur between residues, each $x\in\mathcal R'$ is meant to represent an insertion between residues $x$ and $x+1$. In particular, the sites $0$, $1$, and $\ell$ represent inserts before the initial methionine, after the initial methionine, and after the last residue, respectively.

\begin{definition}
 Let $p_0$ be a protein with a residue set of size $\ell$ in a protein cluster $\mathcal P$ of size $n$. Then the  \emph{multiple sequence alignment} (MSA) of $\mathcal P$ is defined as a matrix $M\in\mathcal A'^{n\times L}$ such that in any column, all non\hyp{}gap amino acids match by descent. The length $L>\ell$ represents the length of the query protein plus all insertions inferred by the alignment algorithm.
\end{definition}

\begin{definition}
 A \emph{gap} in $M$ is a submatrix $G(a,b)=M(1:n,a:b)$ s.t.
 \begin{enumerate}
 \item $a < b$: At least two columns are covered. The residues $a$ and $b$ are referred to as the \emph{flanks}.
 \item $M(1:n,a),M(1:n,b)\in\mathcal A^n$: All residues are matched/mismatched in the flank columns (i.e. no gaps).
 \item $\forall j\in\{a+1,\ldots,b-1\}$, $\exists i$ s.t. $M(i,j) \in\{\texttt{"."},\texttt{"-"}\}$: all internal columns have at least one sequence with a gap
\end{enumerate}
\end{definition}

\begin{definition}
 A \emph{feature} $\operatorname{C}$ is a measure $\operatorname{C}:\mathcal R'\rightarrow\mathbb R$. Given a set of features $\{\operatorname{C}_i\}_{i=1}^k$, a \emph{feature matrix} is defined by the $\ell\times k$ block matrix $\begin{pmatrix}\operatorname{C}_i(\mathcal R')\end{pmatrix}$.
\end{definition}


\section*{Query fetching}
\label{sec:query}
Given the UniProtKB \autocite{Magrane2011} ID for a query protein $p_0$ of length $\ell$, its database record is fetched and parsed using the BioPython \texttt{SeqIO} module  \autocite{Cock2009}. A label vector $t$ of length $\ell$ is constructed using annotations in the record, which are stored as intervals of residues annotated with a string from a preset list of annotation types. For a given residue $x$, its label is the set of text annotations for all intervals in which it is contained.



\section*{Structure determination}
\label{sec:struct}
Secondary structure probabilities and relative surface areas are predicted from the \texttt{FASTA} sequence using the NetSurfP tool  \autocite{Petersen2009}. Five vectors of length $\ell$ corresponding to per\hyp{}residue relative surface area, Z\hyp{}score, helix, strand, and coil probabilities are constructed, respectively.

\section*{Sequence profile training}
\subsection*{Protein cluster search, filtering, and multiple sequence alignment}
\label{sec:homologysearch}

Similar protein search hits, MSAs, and pHMMs are retrieved using the hhblits homology search tool  \autocite{Remmert2012} in \texttt{HHR}, \texttt{A3M}, and \texttt{HHM} formats, respectively, searching against a local copy of the uniprot20 database constructed on 1$^\text{st}$ of September, 2015. The parameters are set to return at most 20000~hits with no sequence diversity restrictions and otherwise default parameters. The hit set and MSA which result from the template HMM (where all sequences in each cluster are at least $20\%$ identical in sequence  \autocite{Remmert2012}) and search results are then filtered to only include those with at least 80\%~coverage of the query sequence.
\label{subsec:msa}
The alignment is then converted to \texttt{A2M} format with the \texttt{reformat.pl} script included in hhsuite  \autocite{Soding2005} to insert gap characters. No other filtration steps are performed. The result is a set of search hits $\mathcal H$ and its corresponding MSA.

\subsection*{Insert length resolving profile hidden Markov model}
\label{subsec:ilrphmm-desc}
Using the post-filtration MSA as input, a pHMM in \texttt{HHM} format is trained using the \texttt{hhmake} command line tool  \autocite{Soding2005}. An ilrp-HMM is constructed by augmenting the pHMM with per\hyp{}residue insert length distributions computed from the MSA.
Using the transition and emission probabilities in the model, length variability and sequence variability, respectively, are computed (details below).


\section*{Permissiveness and tag functionality features}
\label{sec:scoring}
\subsection*{Overview}
Using transition and emission probabilities in the ilrp-HMM and the structural measures, functions predicting site permissiveness and tag functionality are computed to get a \emph{feature function} defined on all residues. In general, features are either probabilities or values normalised to $[0,1]$ using their respective distributions.

More precisely, the scoring criteria or \emph{features} are measures $\{\operatorname{C}_i:\mathcal R\rightarrow[0,1]\}$, which are the components of the \emph{feature function}
\begin{equation*}
 \operatorname{Feat}_i(x;\theta) = \operatorname{C}_i(x;\theta),
\end{equation*}

where $x\in\mathcal R'$ denotes an insert site and $\theta$ denotes the vector of parameters. 
The scoring criteria functions and their parameters are described in the following sections. 

\subsection*{Insertion probability}
\label{sec:ins}
Let $M\in\mathcal A'^{n\times L}$ be an MSA, where $L>\ell$ corresponds to the length of the query sequence after gap insertion. Note that $M_x$ denotes the match state in a pHMM at residue $x$, while $M(i,j)$ denotes the element at row $i$ and column $j$ of an MSA. Let the set of gaps be $\{G(a_i,b_i)\}_i$, where $a_i,b_i\in\mathcal R'$ and $b_i-a_i-1$ are their respective maximum insert lengths.
\subsubsection*{Model 1: Affine gap model}

From the transition probabilities in the pHMM corresponding to gap opening and extension, the probability of an insert at site $x$ with length at least $n$ is
\begin{equation*}
 \Pr[M_x\rightarrow I_x]\Pr[I_x\rightarrow I_x]^{n-1},
\end{equation*}
while the probability of an insert of exactly length $n$ is
\begin{equation*}
 \Pr[M_x\rightarrow I_x]\Pr[I_x\rightarrow I_x]^{n-1}\Pr[I_x\rightarrow M_{x+1}].
\end{equation*}

\subsubsection*{Model 2: Empirical insert length distribution}
To take advantage of all available sequence information, we propose a novel profile hidden Markov model, called the insert length resolving profile HMM (ilrp-HMM), to model the empirical distribution of insert lengths at each residue in the query protein. When computing insert length distributions from the MSA during ilrp-HMM training, a weighting step must first be applied to correct for the uneven sampling of sequences from the protein sequence space \autocite{Altschul2009}. Henikoff weights are a non\hyp{}linear correction method used in MSA algorithms to correct for this bias \autocite{Henikoff1994,Altschul2009}.



To initialize, \emph{weights} are generated for each letter $M(i,j)$ in the MSA, which are defined as
\begin{equation*}
 \operatorname{w}(i,j) = \frac{1}{|\{k : M(k,j)=M(i,j)\}|}.
\end{equation*}
Whether $w$ is defined when $M(i,j) =$ ``-'' is a design choice, but for this method, it is defined. These weights are then normalised to get
\begin{equation*}
 \hat{\operatorname{w}}(i,j) = \frac{\operatorname{w}(i,j)}{\sum_{k=1}^n \operatorname{w}(k,j)}.
\end{equation*}
The \emph{absolute weight} of each sequence $m$ is then defined as
\begin{equation*}
 \operatorname{W}(m) = \sum_{j=1}^L \hat{\operatorname{w}}(m,j),
\end{equation*}
from which the Henikoff weight \autocite{Henikoff1994} of $m$ is defined as
\begin{equation*}
\operatorname{HW}(m) = \frac{\operatorname{W}(m)}{\sum_{i=1}^n \operatorname{W}(i)}.
\end{equation*}

Given a gap $G(a_i,b_i)$, the index set of sequences with an insert of length $k$ is then computed by checking the subsequences of each sequence in the gap window and counting how many of them have a gap of length $k$,
\begin{equation*}
 \operatorname{NS}(k,a_i,b_i) = \left\{i : |\{j\in\{a_i+1,\ldots,b_i-1\} : M(i,j)\neq \texttt{"-"}\}|=k\right\}.
\end{equation*}
 Since the image of $\operatorname{NS}$ for fixed $a_i$ and $b_i$ forms a disjoint partition of $\{1,\ldots,n\}$,
 the probability of an insertion of length $k$ at $G(a_i,b_i)$ is the sum of the weights of all sequences in which an insert of length $k$ is observed
\begin{equation*}
 \Pr[M_{a_i}\rightarrow I_{a_i}^k] = \sum_{m\in\operatorname{NS}(k,a_i,b_i)}\operatorname{HW}(m).
\end{equation*}
Finally, the model assumes that transitions from insertions to deletions do not exist, so for all $k$
\begin{equation*}
 \Pr[I_{a_i}^k\rightarrow M_{a_i+1}] = 1.
\end{equation*}

\subsection*{Deletion probability}
\label{sec:del}
Empirical deletion probabilities can be inferred directly from the transition probabilities of a standard pHMM since each deleted residue has its own state and corresponding transition edges. However, due to the possibility of different deletions overlapping between organisms, care must be taken when incorporating $D\rightarrow D$ transitions. To ensure that the sum of all probabilities for transitions out of deletion states is equal to $1$, given a site $x$, the \emph{maximum deletion length} $\operatorname{DL}$ is defined as
\begin{equation*}
 \operatorname{DL}(x) := \max\left\{k : 1\geq \sum_{j=1}^k\Pr[D_{x+j}\rightarrow M_{x+j+1}]\prod_{i=1}^{j-1}\Pr[D_{x+i}\rightarrow D_{x+i+1}]\right\}
\end{equation*}
So the probability of a deletion of exactly length $k$ is
\begin{equation*}
 \begin{split}
  \Pr[&M_x\rightarrow D^k]=\\
 &\Pr[M_x\rightarrow D_{x+1}]\Pr[D_{x+k}\rightarrow M_{x+k+1}]\prod_{i=1}^{k-1} \mathbb{1}_{i < \operatorname{DL}(x)}\Pr[D_{x+1}\rightarrow D_{x+i+1}],
 \end{split}
\end{equation*}
where $\mathbb{1}$ denotes the binary indicator function.

\subsection*{Length variability}
\label{sec:lengthvar}
Since estimates for probabilities of insertions are limited by the number of similar sequences taken into account, the probability of an indel is expected to become more dependent on search hit set size for longer lengths  \autocite{Benner1993,Strope2006}. In particular, when few similar sequences are available, indels of several lengths will have a probability of $0$ due to a lack of evidence. However, a site which accepts inserts of several lengths is likely to accept longer inserts even if none have been observed  \autocite{Benner1993}. The Shannon \emph{entropy} of the indel length distribution provides a measure of length variability that avoids relying on regions of the distribution that may not be accurate due to a lack of data,

\begin{equation*}
\begin{split}
 \operatorname{C}_1(x;\theta) := &-\sum_{j=1}^L \left(\Pr[M_x\rightarrow I_x^j]\log_2\Pr[M_x\rightarrow I_x^j]\right.\\
 &\left.+ \Pr[M_x\rightarrow D^j]\log_2\Pr[M_x\rightarrow D^j]\right)
\end{split}
\end{equation*}


\subsection*{Sequence variability}
\label{sec:seqvar}

All of the previously described features are measures of length variability in the query protein. An additional measure of mutagenesis tolerance, especially useful in the context of tagging by sequence replacement, is sequence variability. This can also serve as a proxy measure in the absence of good quality domain boundary information \autocite{Johansson2010}.

Given the per\hyp{}residue amino acid frequency distribution $Q(x)$ (computed from the emission probabilities of the HMM models) of the protein cluster $\mathcal P$ and a background distribution $P$, the \emph{conservation} of site $x$ can be defined by several measures, including the Kullback\hyp{}Leibler (KL) divergence, the Jensen\hyp{}Shannon (JS) divergence from $P$ to $Q(x)$, or the Jensen\hyp{}Shannon distance from $P$ to $Q(x)$ \autocite{Lin1991}.

The KL divergence measure is unbounded and order\hyp{}of\hyp{}magnitude differences between extremely small probabilities can lead to large order\hyp{}of\hyp{}magnitude differences in the divergence score \autocite{Lin1991}. This has the potential to lead to scaling issues when evaluated against other features, so a bounded divergence measure is preferable. This issue does not apply to the JS distance (the square root of the JS divergence), which is also bounded by $[0,1]$ when computed from the KL divergence in bits \autocite{Lin1991}. 
%\footnote{A \emph{true metric} has the following properties: 1) non-negative, 2) 0 iff two points are equal, 3) symmetric, and 4) satisfies the triangle inequality.}
Let 
\begin{equation*}
\operatorname{c}_{\mathrm{JSD}}(x)=\sqrt{\operatorname{D}_{\mathrm{JS}}(P\|Q(x))} 
\end{equation*}
denote the JS distance from $P$ to $Q(x)$ with the JS divergence $\operatorname{D}_{\mathrm{JS}}$, defined as
\begin{equation*}
 \operatorname{D}_{\mathrm{JS}}(P\|Q(x)) = \frac{1}{2}\operatorname{D}_{\mathrm{KL}}(P\|M(x)) + \frac{1}{2}\operatorname{D}_{\mathrm{KL}}(Q(x)\|M(x)),
\end{equation*}
with $M(x) = \frac{1}{2}P + \frac{1}{2}Q(x)$, and the KL divergence being
\begin{equation*}
 \operatorname{D}_{\mathrm{KL}}(P\|Q(x)) = \sum_{i=1}^{20} P(i)\log_2\frac{P(i)}{Q(x,i)}.
\end{equation*}
Let $X\sim\operatorname{Beta}\left(\hat\alpha,\hat\beta\right)$ be the distribution of $\operatorname{c}_{\mathrm{JSD}}$ values, where the shape parameters $\hat\alpha$ and $\hat\beta$ are fit by maximum likelihood estimation (MLE). The \emph{sequence variability} and \emph{sequence conservation} of $x$ are then defined as $\Pr\left[X > \operatorname{c}_{\mathrm{JSD}}(x)\right]$ and $\Pr\left[X < \operatorname{c}_{\mathrm{JSD}}(x)\right]$, respectively.

\subsubsection*{Amino acid similarity\hyp{}corrected sequence conservation}
In this definition of divergence, the contributions to the summation of each amino acid type are equal. This approach, however, does not take into account the fact that certain amino acid substitutions are more likely than others due to functional similarities  \autocite{Henikoff1992}. A weighting scheme based on observed amino acid substitution rates incorporated into the sequence variability feature function can correct for this. Starting with substitution matrix $S\in[0,1]^{20\times20}$ (e.g. BLOSUM62 \autocite{Henikoff1992}) defined as
\begin{equation*}
 S_{i,j} = \Pr[AA_i\rightarrow AA_j],\text{ where }AA_i \in \mathcal A,
\end{equation*}
we define the \emph{$S$\hyp{}weighted KL divergence} as
\begin{equation*}
\begin{split} 
 \operatorname{D}_{\mathrm{KLS}}(P\|Q(x)) := \sum_{i=1}^{20} &\left((1+S_{i,i})P(i)\log_2\frac{P(i)}{Q(x,i)}\right. \\
 &\left.- \sum_{j=1}^{20} S_{i,j}P(j)\log_2\frac{P(j)}{Q(x,j)}\right),
 \end{split}
 \end{equation*}
 which subtracts the contributions of each amino acid from the divergence depending on how similar they are to other amino acids. If $S$ is symmetric, this simplifies to
 \begin{equation*}
   \operatorname{D}_{\mathrm{KLS}}(P\|Q(x)) = \sum_{i=1}^{20} S_{i,i}P(i)\log_2\frac{P(i)}{Q(x,i)}
 \end{equation*}
Using this, we define the $S$\hyp{}weighted JS distance as
\begin{align*}
 \operatorname{c}_{\mathrm{JSDS}}(x) &:= \sqrt{\operatorname{D}_{\mathrm{JSS}}(P\|Q(x))} 
 \\&= \sqrt{\frac{1}{2} \operatorname{D}_{\mathrm{KLS}}(P\|M(x)) + \frac{1}{2}\operatorname{D}_{\mathrm{KLS}}(Q(x)\|M(x))}
 \\&= \sqrt{\frac{1}{2}\sum_i S_{i,i}\left(P(i)\log_2\frac{P(i)}{M(x,i)} + Q(i)\log_2\frac{Q(x,i)}{M(x,i)}\right)}.
\end{align*}

Following the same beta distribution correction applied above, the final sequence variability feature function becomes
\begin{equation*}
 \operatorname{C}_2(x;\theta) := \Pr[X > \operatorname{c}_{\mathrm{JSDS}}(x)].
\end{equation*}

\subsection*{Secondary structure}
\refstepcounter{secstr}\label{sec:secstr}
Since some tags, such as the TEV protease cleavage tag, require their insertion site to be in a region free of secondary structure -- i.e., in a loop rather than an $\alpha$-helix or $\beta$-strand -- for proper cleavage \autocite{Phan2002,Eser2007}, and since the fold of the query protein should be preserved as much as possible, a probabilistic measure of secondary structure taking into account the length of the local coil (non\hyp{}helix or strand) is introduced. Namely, the measure estimates the probability that an indel at residue $x$ will disrupt the secondary structure at a site $\operatorname{s}(x)$ residues away, where $\operatorname{s}:\mathcal R\rightarrow\N$ is the distance to the nearest non\hyp{}coil structural element.

Let $\rho$ be the probability that a change in structure at $x$ affects the structure of its flanking residues $\{x-1,x+1\}$. Assuming an exponential decay model, a change in $x$ thus has a probability $\rho^k$ of affecting the residues $\{x-k,x+k\}$. Assuming that these changes are not dependent on the length of an inserted tag, under this model, the probability that an insertion at $x$ will affect the nearest secondary structure is $\rho^{s(x)}$. 

Using this, the probability of a change at $x$ not affecting secondary structure at distance $\operatorname{s}(x)$ is
\begin{equation*}
 \operatorname{C}_3(x;\theta) := \operatorname{S}(x;M_s) = 1-e^{\frac{-\operatorname{s}(x)-1}{M_s}},
\end{equation*}
with a decay parameter $M_s := \frac{-1}{\log\rho}$. An extra multiplication factor of $e^{\frac{-1}{M_S}}$ is applied to ensure that $\operatorname{S}(x;M_s)>0$. Since mutations have little effect on regions more than 8 residues away  \autocite{Chen2006}, a value of $M_s = 2$ can be set to ensure that the probability of affecting regions more than 7 residues away is less than $2\%$.

\subsubsection*{Incorporating secondary structure probabilities from predictions}
\label{sec:secstructunc}
Suppose instead that probabilities of secondary structure are given instead as a map
\begin{equation*}
 \operatorname{p}:\mathcal R\rightarrow [0,1]^3
\end{equation*}
from residues to the probabilities of helix, strand, and coil, respectively
\begin{equation*}
 \operatorname{p}(x) = \left(\operatorname{p_{H}}(x),\operatorname{p_S}(x),\operatorname{p_C}(x)\right).
\end{equation*}
It is desirable to define a new score $\hat{\operatorname{S}}(x;M_s)$ that takes this into account to not penalise sites with uncertain predictions. For consistency, it is also desirable for the function to be equivalent to $\operatorname{S}$ when given perfect information.

A new minimal\hyp{}distance\hyp{}to\hyp{}secondary\hyp{}structure function $\hat{\operatorname{s}}:\mathcal R\rightarrow\N$ incorporating $\operatorname{p}$ is defined as
\begin{equation*}
 \hat{\operatorname{s}}(x) = \min_{y\in\mathcal R}\left\{|x-y| : (\operatorname{p_H}(y) > \operatorname{p_C}(y)) \vee (\operatorname{p_S}(y) > \operatorname{p_C}(y))\right\}
\end{equation*}
(i.e. pick the most likely structure). For each site $x$, this induces the \emph{neighbourhood} of $x$
\begin{align*}
 \operatorname{n}(x) &= \{x-\hat{\operatorname{s}}(x)+1,\ldots,x+\hat{\operatorname{s}}(x)-1\}\cap\mathcal R
 \\&=\{\max(x-\hat{\operatorname{s}}(x)+1,1),\ldots,\min(x+\hat{\operatorname{s}}(x)-1,\max\mathcal R)\}
\end{align*}
from which the \emph{local coiliness} of $x$ can be defined
\begin{equation*}
 \operatorname{LC}(x) = \left(\displaystyle\prod_{j\in \operatorname{n}(x)} \operatorname{p_C}(j)\right)^{\frac{1}{|\operatorname{n}(x)|}}
\end{equation*}
(i.e. the geometric mean of the region's coil probabilities).

Finally, this is incorporated into $\operatorname{S}$ to define an uncertainty-corrected secondary structure feature function
\begin{equation*}
 \operatorname{C}_3'(x;\theta) := \hat{\operatorname{S}}(x;M_s) = 1-e^{\frac{-\hat s(x)\operatorname{LC}(x)-1}{M_s}}
\end{equation*}
(since $\operatorname{LC}(x)=1$ when secondary structure probabilities are certain, this new feature function reduces to the crystal structure-based measure in this case). Under this model, however, a discontinuity exists between the feature values for $x=0$ and $x=1$, while for values above $0$, the smoothing from the local coiliness function $\operatorname{LC}$ results in a more uniform spread of values (see Supplentary Figure \ref{fig:secstruct_dist}).

\begin{figure}
 \includegraphics[width=0.85\textwidth]{Figures/secstruct.pdf}
 \caption{\textbf{Probability of preserving the nearest non\hyp{}coil element for all residues in the query proteins.} $60\%$ of residues are in an $\alpha$\hyp{}helix or a $\beta$\hyp{}strand.
 The probability is defined with an exponential decay model where the decay rate is calibrated so that the probability of preserving a structural element 8 residues away is greater than $0.98$ (see section ``\ref{sec:secstr}'' for a derivation of the function). The peak at $0.39$ corresponds to the probability of preserving the secondary structure of the residues immediately flanking an insertion site. Secondary structure predictions were made by the NetSurfP tool \autocite{Petersen2009} for all proteins annotated as being in the \emph{E. coli K12} proteome in the UniProtKB/Swiss-Prot database \autocite{Magrane2011}.
 }
 \label{fig:secstruct_dist}
\end{figure}

\subsection*{Surface accessibility}
\label{sec:rsas}
While measures of surface accessibility, such as absolute and relative surface area, are more readily definable compared to secondary structure\hyp{}based measures, an accessibility measure that more readily fits within a probabilistic framework is desirable. Rather than defining the accessibility score of a site $x$ as being the relative surface area of its flanking residues, one may compute the probability of $x$ being less exposed than other sites, which we call its \emph{relative surface accessibility} (RSA).

Let $\operatorname{rsa}:\mathcal R\rightarrow[0,1]$ be a map from a residue to its relative surface area. Since inserts occur between residues, the RSA of a site should be intermediate between the two flanking residues. Since these are normalised values, the relative surface area of a site $x\in\mathcal R'$ can be defined as the geometric mean of its flanking residues' values,
\begin{equation*}
 \operatorname{sitersa}(x) = \begin{cases}
                               \sqrt{\operatorname{rsa}(x)\operatorname{rsa}(x+1)} & \text{if }0<x < \ell\\
                               \operatorname{rsa}(1) & \text{if }x=0\\
                               \operatorname{rsa}(\ell) & \text{if }x=\ell
                              \end{cases}.
\end{equation*}

To convert this into a probabilistic measure, the distribution of $\operatorname{sitersa}$'s image is fit to $X\sim\operatorname{Beta}(\hat\alpha,\hat\beta)$ by MLE. From this, the RSA feature function is defined as
\begin{equation*}
 \operatorname{C}_4(x;\theta) := \operatorname{RSA}(x) = \Pr[X<\operatorname{sitersa}(x)]
\end{equation*}

\subsubsection*{Incorporating uncertainty from predictions}
To avoid penalizing sites with uncertain relative surface area predictions, prediction certainty (measured by Z\hyp{}scores $R$  \autocite{Petersen2009}) is incorporated. Define $\operatorname{Pcor}$ as the density of $\mathcal N(\mu,\sigma^2)$ covered by $[-\infty,\mu+|R|\sigma]$,
\begin{equation*}
 \operatorname{Pcor} = \frac{1}{2}\left(1+\operatorname{erf}\left(\frac{|R|}{\sqrt{2}}\right)\right)\in[0.5,1),
\end{equation*}
which defines a probabilistic uncertainty function. This induces the uncertainty\hyp{}corrected RSA feature function
\begin{equation*}
 \operatorname{C}_4'(x;\theta) := \Pr[X<\operatorname{RSA}(x)]^{\operatorname{Pcor}},
\end{equation*}
which is used in the final version of the classifier.

In the case where $R=0$, $\operatorname{Pcor}=0.5$, so the square\hyp{}root of the probability is taken (less penalization due to uncertainty). As $|R|\rightarrow\infty$, which represents the case in which a crystal structure is available (the accessibility is certain), the correction limits to $1$ (no correction)
\begin{equation*}
 \lim_{|R|\rightarrow\infty}\frac{1+\operatorname{erf}\left(\frac{|R|}{\sqrt{2}}\right)}{2}=1.
\end{equation*}

\section*{Taggability classification}
\subsection*{Taggability labels}
\label{sec:labels}

Taggability labels were derived from literature data in both automated and manual fashions. The majority of the ``untaggable'' (\emph{negative}) labels were obtained from annotations in the UniProtKB entries of the query proteins \autocite{Magrane2011}. A site was labeled as ``untaggable'' if its annotation list had at least one annotation from the set \{\texttt{binding site}, \texttt{active site}, \texttt{DNA-binding region}, \texttt{site}, or \texttt{metal-ion binding site}\}.

The ``taggable'' (\emph{positive}) and some of the negative labels were obtained from internal protein tagging literature (see Supplementary Table 1). A site was labeled positively if the site was reported as preserving at least $80\%$ of wild\hyp{}type function by some measure and if the tag was functional. All other tested sites were labeled as negative. In the case of conflicting annotations, positive labels reported in the literature took precedence over negative labels retrieved from UniProtKB.


\subsection*{Classifier evaluation}
The dataset was partitioned into training ($80\%$) and test ($20\%$) sets in a stratified manner to ensure that the imbalance ratio was equal in both sets. Classification methods were evaluated by the areas under their receiver operating characteristic (AUROC) \autocite{Hanley1982} and class-weighted precision recall (AUWPR) \autocite{Keilwagen2014} curves computed on the test set. For each value of a classifier's decision function as a decision threshold, the corresponding true and false positive rates (TPR and FPR, respectively) are computed to construct the ROC curve. The weights on data points for computing precision (PPV) and recall (TPR) were set to one-tenth of the imbalance ratio to prevent all sites from being classified as untaggable. To optimize hyperparameters, 10-fold cross-validation with stratified splitting was done on the training set, setting the validation set size to $20\%$ \autocite{Ross2009}. Validation performance was computed as the mean AUWPR measure.

\subsection*{Binary classifiers}
\label{sec:cv}
For binary classification, a balanced random forest \autocite{Chen2004} model was used due to its suitability for imbalanced data. As baseline models, a decision tree \autocite{Rokach2007} was trained and the values of each feature were also used as standalone decision functions. Due to the large imbalance ratio in the training data, a forest of size $2000$ was used to ensure that the set of untaggable sites is adequately represented. To explore structural restrictions for preventing overfitting in the balanced random forest and decision tree classifiers, the hyperparameters defining the maximum tree depth and the minimum number of data points at a leaf node were searched in the $1$ to $6$ and $1$ to $4$ ranges, respectively. The maximum number of features used in each decision was fixed at the total number of features. 

The importance of each feature in the random forest was computed as their respective mean Gini importances among the decision trees \autocite{Breiman2001}, which is defined as follows. At each internal node, the Gini impurity is a measure of the mixture of positive and negative data points in the dataset. When a feature is used to split, the impurity in each child node is strictly less than its parent node. The Gini importance of a feature is then the sum of the differences in Gini impurity caused when that feature is used to split a set at a node. Thus, it is a measure of the discriminative power of that feature.

\section*{Dataset}
Features were computed for all proteins in the UniProtKB/Swiss-Prot \autocite{Magrane2011} database retrieved on 2016.03.23 and annotated as being in the \textit{Escherichia coli K12} proteome. Features were also computed for the \textit{E.~coli} proteins (lacking K12 annotation) TEM1 beta\hyp{}lactamase (UniProtKB ID \uniprot{P62593}), outer membrane usher protein FaeD \uniprot{P06970}, and chloramphenicol acetyltransferase \uniprot{P62577} to take advantage of tagging experiment results from the literature.
