#!/usr/bin/env python3

import urllib.error
import urllib.request
import os.path
import sys
import pickle

import re
import numpy as np
import scipy.special
import scipy.stats

from Bio.SeqIO.UniprotIO import UniprotIterator
from Bio.SeqIO import parse as SeqIOParse
from Bio.SeqUtils.ProtParamData import kd as kd_hydro

from io import StringIO
from bs4 import BeautifulSoup

from collections import OrderedDict

import PDBstruct
from netsurfp import NetSurfP

from Profiles import _base_log_prob

hydro_index = np.full(ord("Z"),np.nan,dtype=float)
for a in kd_hydro:
    hydro_index[ord(a)] = kd_hydro[a]

def get_annotated_query(prefix,email,isoform=None,annotate=True,pdbid=None,nsurfp=True,protein_db=None,debug=False,db_dict=None):
    query_id = prefix[prefix.rfind("/")+1:]
    seqquery = fetch_query(query_id,prefix,isoform=isoform,db_dict=db_dict)
    if annotate:
        _annotate_query(prefix,seqquery,email,pdbid,nsurfp,protein_db,debug)
    return seqquery
    
def fetch_query(query_id,prefix=None,isoform=None,db_dict=None):
    """Retrieves files from SwissProt given a SwissProt id. Writes as fasta format to query_file"""
    #QUERY STUFF
    #get query sequence
    if db_dict is None:
        #if no dictionary provided, fetch from the interwebs
        if not prefix or not os.path.isfile(prefix+".xml"):
            try:
                query_lines = urllib.request.urlopen("http://www.uniprot.org/uniprot/"+query_id+".xml").read().decode("ascii")
            except urllib.error.HTTPError:
                raise OptionValueError(query_id+" is not a valid UniProt id")
            #write SwissProt xml to file
            if prefix:
                with open(prefix+".xml","w") as swissout:
                    swissout.write(query_lines)
        elif prefix:
            with open(prefix+".xml","r") as f:
                query_lines = f.read()
        seqquery = next(UniprotIterator(StringIO(query_lines)))

        #replace sequence with an isoform
        if isoform:
            try:
                isoform_lines = urllib.request.urlopen("http://www.uniprot.org/uniparc/?direct=yes&format=fasta&query=isoform:"+query_id+"-"+isoform).read().decode("ascii")
                #isoform_lines = urllib.request.urlopen("http://www.uniprot.org/uniparc/?direct=yes&format=xml&query=isoform:"+query_id+"-"+isoform).read().decode("ascii")
            except urllib.error.HTTPError:
                raise OptionValueError("Invalid isoform ID")
            seqquery.annotations['isoform_id'] = isoform
            seqquery_isoform = next(SeqIOParse(StringIO(isoform_lines),"fasta"))
            if len(seqquery_isoform) == len(seqquery):
                letannot = seqquery.letter_annotations
                seqquery.seq = seqquery_isoform.seq
                seqquery.letter_annotations = letannot
            else:
                raise OptionValueError("For now, only isoforms that are the same length as the canonical form are supported")
    else:
        #fetch from database
        print("from local db")
        if isinstance(db_dict,str):
            db_dict_file = db_dict
            #it links to a pickle file
            with open(db_dict_file,"rb") as dfile:
                try:
                    db_dict = pickle.load(dfile)
                except:
                    raise Exception("Path does not link to a well-formed pickle")

        if isinstance(db_dict,dict) and query_id in db_dict:
            seqquery = db_dict[query_id]
        else:
            raise Exception("UniProt ID not found in provided database or database not a dictionary")
    
    #write query FASTA file 
    if prefix:
        with open(prefix+".fasta","w") as fasta_out:
            fasta_out.write(seqquery.format("fasta"))
    return seqquery

def _annotate_query(prefix,seqquery,email,pdbid=None,nsurfp=True,protein_db=None,debug=False):
    """annotate query sequence from PDB, netsurfp, interproscan"""
    netsurfp_results = prefix+".netsurfp.out"
    #list of associated PDB ids. then checks if the provided ID is one of them
    pdbs = [a[4:].upper() for a in seqquery.dbxrefs if a[:3] == "PDB"]
    if pdbid is None:
        print("No PDBID selected. Structural information will be predicted from the sequence.")
    elif pdbid:
        pdbid_sp = pdbid.split(":")
        pdbid,chain = (":".join(pdbid_sp[:-1]).upper(),pdbid_sp[-1]) if len(pdbid_sp)>1 else (pdbid_sp[0].upper(),None)
        if pdbid not in pdbs:
            print("PDB ID not in list of known PDBs for this protein, errors may occur.",file=sys.stderr)

    #netsurfp should be done first, since a correlation between surface exposure measures is computed in the PDB method
    if nsurfp:
        nsurf = NetSurfP(prefix+".fasta",netsurfp_results,nsurfp=nsurfp,protein_db=protein_db,remote_warning=False,query_length=None)
        nsurf.run()
        nsurf.fetch_results()
        nsurf.annotate_fasta(seqquery)
    if pdbid:
        PDBstruct.annotate_fasta(pdbid,chain,seqquery,prefix,debug)

    #hydrophobicity index
    seq = np.array(list(str(seqquery.seq).encode()),dtype=int)
    seqquery.letter_annotations['hydrophobicity_index'] = hydro_index[seq]

    #annotations
    annotate_sites_bin(seqquery)
    
def _fetch_article_abstracts(seqquery):
    #Fetch all articles
    articles = [b['MedlineCitation'] for b in Entrez.read(Entrez.efetch(db="pubmed",id=[a.pubmed_id for a in seqquery.annotations['references']],rettype="abstract"))]

    #Extract all abstracts from the articles
    abstracts = [a['Article']['Abstract']['AbstractText'][0] for a in articles]
    return zip(articles,abstracts)


def annotate_sites_bin(seqquery):
    region_id_keys = [\
                        "active site",
                        "binding site",
                        "site", 
                        "metal ion-binding site",
                        "DNA-binding region",
                        "glycosylation site",
                        "nucleotide phosphate-binding region",
                        "cross-link",
                        "zinc finger region",
                        "calcium-binding region",
        ]

    #convert to dictionary for easy indexing
    region_ids = OrderedDict((b,a) for a,b in enumerate(region_id_keys))

    #since stored row-major, faster to make modifications this way
    bins = np.zeros(shape=(len(region_ids), len(seqquery)),dtype=bool)
    for feat in seqquery.features:
        if feat.type in region_ids:
            bins[region_ids[feat.type],int(feat.location.start):int(feat.location.end)] = 1
    seqquery.annotations['regions_bin'] = bins
    return bins,region_id_keys


def score_secondary_structure(seqquery,ms,force_predictions=False):
    #compute score from predictions if crystal not available or if forced
    only_predictions = ("secondary_structure" not in seqquery.letter_annotations) or force_predictions
    if only_predictions:
        hprob = seqquery.letter_annotations['helix_prob_predicted']
        sprob = seqquery.letter_annotations['strand_prob_predicted']
        cprob = seqquery.letter_annotations['coil_prob_predicted']
        #binary vector, 0 if coil, 1 otherwise
        sstruct = ((cprob <= hprob) | (cprob <= sprob)).astype(int)
    else:
        #binary vector, 0 if coil, 1 otherwise
        sstruct = np.in1d(np.fromiter(iter(seqquery.letter_annotations["secondary_structure"]),dtype="S1",count=len(seqquery)),np.array(['H','E'],dtype="S1")).astype(int)
    
    #positions of helices and strands
    sstruct_nc = np.nonzero(sstruct)[0]

    #distance to nearest structure function
    dist_func = np.fromiter((np.min(np.abs(sstruct_nc - i)) for i in range(len(sstruct))),dtype=float,count=len(sstruct)) if len(sstruct_nc) else np.full(len(sstruct),len(sstruct),dtype=float)

    if only_predictions:
        #uncertainty correction: take into account probability of coil, compute geometric mean in log space
        lc = np.fromiter((np.mean(np.log(cprob[int(max(i-a+1,0)):int(min(i+a,len(dist_func)))] if a else 1)) for i,a in enumerate(dist_func)),dtype=float,count=len(dist_func))
        dist_func *= np.exp(lc)

    return -np.expm1((-dist_func-1)/ms)

def score_secondary_structure_log(seqquery,ms,force_predictions=False):
    return np.log(score_secondary_structure(seqquery,ms=ms,force_predictions=force_predictions))

def score_rsa_log(seqquery,rsa_fit,force_predictions=False,correct=True):
    rsa_values,zscores = (seqquery.letter_annotations["surface_exposure_dssp"],\
                          np.inf) \
                    if "surface_exposure_dssp" in seqquery.letter_annotations and not force_predictions\
                    else (seqquery.letter_annotations['relative_solvent_accessibility_predicted'],\
                          seqquery.letter_annotations['rsa_zscore'])

    #geometric mean of RSAs
    site_rsa = np.concatenate([np.sqrt(rsa_values[:-1]*rsa_values[1:]),[rsa_values[-1]]]).reshape(1,-1)
#    site_rsa = np.sqrt(rsa_values * np.pad(rsa_values[1:],(0,1),mode="constant"))
    site_rsa_probs = scipy.stats.beta.logcdf(site_rsa,*rsa_fit)
    #correct for RSA = 0
    site_rsa_probs[site_rsa_probs == -np.inf] = _base_log_prob
    uncertainty_correction = 2/(1+scipy.special.erf(np.abs(zscores)/np.sqrt(2))) if correct else 1
    return site_rsa_probs*uncertainty_correction

def score_rsa(seqquery,rsa_fit,force_predictions=False):
    return np.exp(score_rsa_log(seqquery,rsa_fit,force_predictions=force_predictions))

def score_rsa_slices_log(slices,seqquery,rsa_fit,force_predictions=False):
    site_rsa_scores = score_rsa_log(seqquery,rsa_fit,force_predictions=force_predictions)
    return np.fromiter((np.max(site_rsa_scores[sl]) for sl in slices),dtype=float,count=len(slices))

def score_rsa_slices(slices,seqquery,rsa_fit,force_predictions=False):
    site_rsa_scores = score_rsa(seqquery,rsa_fit,force_predictions=force_predictions)
    return np.fromiter((np.max(site_rsa_scores[sl]) for sl in slices),dtype=float,count=len(slices))


if __name__ == "__main__":
    seqquery = get_query()

