#!/usr/bin/env python3

import requests
import time
from bs4 import BeautifulSoup, SoupStrainer
import re
import os
import numpy as np
import sys
import itertools
from Bio.Application import _Option, _Switch, AbstractCommandline

#secondary structure annotation symbols
_annots = np.array(['H','E',' '],dtype="S1")
_seannots = np.array([' ','b'],dtype="S1")
#report file parsers
_re_splitter = re.compile("\s+")
_select_lines = ['E ','B ']
#XML parsing filters
_linkExt = SoupStrainer('a')
_codeExt = SoupStrainer("pre")

class NetSurfP:
    """NetSurfP job submitter and result parser"""
    def __init__(self,query_file,results_file,nsurfp,protein_db,remote_warning=True,query_length=None):
        self._query_file = query_file
        #variables for running nsurfp locally
        self._nsurfp = nsurfp
        self._protein_db = protein_db
        #Surface predictions
        self._surpreds = None
        self._jobid = ""
        self._results_file = results_file
        #if length of protein is known a priori, then the array to store it can be preallocated
        self._length = query_length
        if os.path.isfile(results_file):
            t = time.time()
            print("Using previously computed NetSurfP results")
            with open(results_file,"r") as f:
                self._rawresults = f.read()
            self.fetch_results()
        else:
            self._rawresults = ""
            if remote_warning:
                print("WARNING: results have not been retrieved or parsed yet",file=sys.stderr)

    def run(self,force=False):
        """Submit a query"""
        #submit job to NetSurfP website if not run already or if forced
        if self._rawresults == "" or force:
            if self._nsurfp == "remote":
                self._starttime = time.time()
                print("Running NetSurfP from server")
                with open(self._query_file,"r") as f:
                    resp = requests.post("http://www.cbs.dtu.dk/cgi-bin/webface2.fcgi",data={"configfile":"/usr/opt/www/pub/CBS/services/NetSurfP-1.1/NetSurfP.cf"},files={"uploadfile":f}).text
                #store URL of job id
                self._jobid = BeautifulSoup(resp,"lxml",parse_only=_linkExt).findAll("a")[0].get("href")
                print("NetSurfP job:",self._jobid)
            else:
                print("Using local copy of NetSurfP")
                if not self._protein_db:
                    raise Exception("BLAST protein database prefix not given. This error should have been caught earlier. Please report this as a bug.")
                cline = NetSurfPCommandline(cmd=self._nsurfp,input=self._query_file,output=self._results_file,secondary_structure=True,protein_db=self._protein_db)
                netsurfp_stdout, netsurfp_stderr = cline()
                try:
                    assert netsurfp_stderr.find("ERROR!") == -1
                except:
                    raise Exception("Error in NetSurfP output\n"+netsurfp_stderr)
                with open(self._results_file,"r") as f:
                    self._rawresults = f.read()

    
    def fetch_results(self,wait=20):
        """Fetch results from server. Wait before the results come in"""
        #check if job has completed. if instructed, keep checking at regular interval
        while self._rawresults == "" and wait:
            try:
                assert self._nsurfp == "remote"
            except:
                raise Exception("Empty NetSurfP result file. Please rerun with force NetSurfP option.")
            code = BeautifulSoup(requests.get(self._jobid).text,"lxml",parse_only=_codeExt).findAll("pre")
            if len(code):
                self._endtime = time.time()
                print("Job ran in " + str(self._endtime-self._starttime) + " seconds.")
                self._rawresults = code[0].getText()
                with open(self._results_file,"w") as f:
                    f.write(self._rawresults)
            else:
                print("Waiting for results from NetSurfP...")
                time.sleep(wait)
        if self._rawresults != "" and self._surpreds is None:
            self._surpreds = ()
            self._length = 0
            for a in self._rawresults.strip().split('\n'):
                if a[:2] in _select_lines:
                    self._length += 1
                    line = _re_splitter.split(a)
                    line_proc = itertools.chain((line[0] == "B",ord(line[1]),np.nan,float(line[3])-1),(float(a) for a in line[4:]))
                    self._surpreds = itertools.chain(self._surpreds,line_proc)
            self._surpreds = np.fromiter(self._surpreds,dtype=float,count=self._length*10).reshape(self._length,10)

    def results_url(self):
        """Returns the url of the online report"""
        return self._jobid

    def raw_results(self):
        """Returns the raw output from NetSurfP"""
        return self._rawresults


    def surface_exposure(self):
        """Return string of single-letter surface exposure predictions"""
        return _seannots[self._surpreds[:,0].astype(int)].tostring().decode()
    def query_seq(self):
        return self._surpreds[:,1].astype(np.uint8).view('S1').tostring().decode()
    def sec_struct(self):
        """Return string of secondary structure predictions"""
        return _annots[np.argmax(self._surpreds[:,7:10],axis=1)].tostring().decode()

    def rasa(self):
        return self._surpreds[:,4]
    def asa(self):
        return self._surpreds[:,5]
    def zscore(self):
        return self._surpreds[:,6]
    def phelix(self):
        return self._surpreds[:,7]
    def pstrand(self):
        return self._surpreds[:,8]
    def pcoil(self):
        return self._surpreds[:,9]

    def annotate_fasta(self,seqquery):
        """Annotate query sequence with surface exposure and secondary structure predictions"""
        #Fetch and parse results if they have not
        if self._surpreds is None:
            self.fetch_results()
        #Check if the NetSurfP result sequence matches the input query sequence
        try:
            inpquery = str(seqquery.seq)
            nspquery = str(self.query_seq())
            assert len(inpquery) == len(nspquery)
            querydiff = "".join(" " if a==b else b for a,b in zip(iter(inpquery),iter(nspquery)))
            assert querydiff.replace("X"," ").strip() == ""
            #assert self.query_seq() == str(seqquery.seq)
        except:
            raise Exception("NetSurfP query does not match this query sequence")
        seqquery.letter_annotations['surface_exposure_predicted'] = self.surface_exposure()
        seqquery.letter_annotations['relative_solvent_accessibility_predicted'] = self.rasa()
        seqquery.letter_annotations['rsa_zscore'] = self.zscore()
        seqquery.letter_annotations['helix_prob_predicted'] = self.phelix()
        seqquery.letter_annotations['strand_prob_predicted'] = self.pstrand()
        seqquery.letter_annotations['coil_prob_predicted'] = self.pcoil()
        #Determine secondary structure from annotation with highest probability
        seqquery.letter_annotations['secondary_structure_predicted'] = self.sec_struct()

    def get_prediction(self,ind):
        """Return predictions for the amino acid at index ind"""
        if not len(self._surpreds[ind]):
            self.fetch_results()
        return self._surpreds[ind]

class NetSurfPCommandline(AbstractCommandline):
    def __init__(self, cmd="netsurfp", **kwargs):
        self.parameters = [
            _Option(['-i','input'],
                    "FASTA input file",
                    filename=True,
                    equate=False,
                    is_required=True,
                    ),
            _Switch(['-a','secondary_structure'],
                    "Compute secondary structure"
                   ),
            _Option(['-o','output'],
                    "RSA output file",
                    filename=True,
                    equate=False,
                    is_required=True,
                    ),
            _Option(['-d','protein_db'],
                    "BLAST protein DB prefix",
                    filename=True,
                    equate=False,
                    is_required=True,
                    ),
            ]
        AbstractCommandline.__init__(self, cmd, **kwargs)

def netsurfp_run(fasta_file,out_file):
    nsurf = NetSurfP(fasta_file,out_file)
    nsurf.run()
    nsurf.fetch_results()

if __name__ == "__main__":
    #netsurfp_run(sys.argv[1],sys.argv[2])
    exit()

