#!/usr/bin/env python3
import pickle
import numpy as np
import scipy.misc
from Bio.SeqIO.UniprotIO import UniprotIterator
from io import StringIO
from Homologs import XMLParser
import lxml.etree as ET

def setup_uniprot_db(*args):
    db_file = "UniprotSeqRecords.p"
    with open(db_file,"wb") as dfile:
        pickle.dump(read_dbs(*args),dfile)

def read_dbs(*args):
    """Given UniProtKB XML files, return a dict mapping IDs to SeqRecords"""
    header = '<?xml version=\'1.0\' encoding=\'UTF-8\'?>\n<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/support/docs/uniprot.xsd">'
    tail = '<copyright>\nCopyrighted by the UniProt Consortium, see http://www.uniprot.org/terms\nDistributed under the Creative Commons Attribution-NoDerivs License\n</copyright>\n</uniprot>\n'

    uniprot_dict = dict()
    for xml in args:
        if isinstance(xml,str):
            curfile = XMLParser(xml,["entry","{http://uniprot.org/uniprot}entry"])
            for entry in curfile.iterator():
                seqquery = next(UniprotIterator(StringIO("".join([header,ET.tostring(entry).decode(),tail]))))
                uniprot_dict[seqquery.id] = seqquery
        else:
             raise Exception("Argument " + str(xml) + " is not a file path")
    return uniprot_dict

