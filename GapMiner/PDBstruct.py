#!/usr/bin/env python3

#Fetches a PDB, then annotates coordinates based on PDB features
from Bio.PDB.PDBList import PDBList
from Bio import pairwise2
from Bio.SubsMat.MatrixInfo import blosum62 as matrix
from Bio.Data.SCOPData import protein_letters_3to1 as three_to_one
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Polypeptide import PPBuilder
from Bio.PDB.HSExposure import ExposureCN,HSExposureCA
from Bio.PDB.DSSP import DSSP
from Bio.PDB import make_dssp_dict
from Bio import SeqIO
from io import StringIO
import urllib.request
import json

from collections import OrderedDict
import copy
import sys
import tempfile
__tempdir__ = tempfile.gettempdir()+"/"
jsonenc = json.JSONEncoder()

import numpy as np

def annotate_fasta(pdbid,chain=None,seqquery=None,prefix=__tempdir__,dsspfile=None,debug=False):
    """Annotates SeqRecord based on PDB structure secondary information"""

    pdb_file_name = fetch_pdb(pdbid,prefix[:prefix.rfind("/")])
    with open(pdb_file_name,"r") as pdb_file:
        structure,structure_chains,seqquery = get_pdb_seqrecord(pdb_file,pdbid,chain,pdb_file_name,seqquery,debug)
        _annotate(seqquery,structure,structure_chains,dsspfile)
    return seqquery

def fetch_pdb(pdbid,pdbdir):
    pdblist = PDBList(server=PDBList.alternative_download_url,pdb=pdbdir)
    return pdblist.retrieve_pdb_file(pdbid,pdir=pdbdir)

def export_dssp_to_JSON(seqquery):
    annot = {\
        'secondary_structure_dssp': np.fromiter(iter(seqquery.letter_annotations['secondary_structure']),dtype="S1"),
        'rsa_dssp'                : seqquery.letter_annotations['surface_exposure_dssp'],
    }
    annot['secondary_structure_dssp'][annot['rsa_dssp'] == np.NaN] = b"-"
    annot['secondary_structure_dssp'] = annot['secondary_structure_dssp'].tostring().decode()
    annot['rsa_dssp'] = annot['rsa_dssp'].tolist()
    json_data = jsonenc.encode(annot).replace("NaN","null")
    return json_data


def get_pdb_seqrecord(handle,pdbid,chain,pdb_file_name,seqquery=None,debug=False):
    """Extracts FASTA from PDB header. If a query sequence is provided, annotate the query with the pdb Seq. Otherwise, return the pdb Seq"""
    
    if chain:
        chains = chain.split(",")
    
    #Extract all chains, then construct sequence from the ones selected by user
    #Ignore PDB construction warnings
    parser = PDBParser(QUIET=(not debug))
    ppb = PPBuilder()
    structure = parser.get_structure(pdbid,handle)[0]
    pdb_struct = structure.get_parent()
    structure_allchains = list(structure.get_chains())
    peptides_all = ((a.id,ppb.build_peptides(a)) for a in structure_allchains)


    #map from chain to (map from PDB coord to residue)
    allcoords = dict( \
            (ch, OrderedDict( (curid[2:4],s)  for b in c for curid,s in zip((a.get_full_id() for a in b.get_ca_list()),b.get_sequence()) )) \
            for ch,c in peptides_all)

    #select all chains if none selected
    if not chain:
        chains = sorted(allcoords.keys())
        chain = ",".join(chains)

    #construct set of unique coordinates (for each residue, only take one copy from some chain)
    #map from coord of form ("X","pos","X") to tuple (chain,residue)
    uniqcoords = OrderedDict()
    for ch in sorted(allcoords.keys()):
        for a in allcoords[ch]:
            #add a new coordinate if not previously found, or if not found in a chain of interest
            if not a[1] in uniqcoords or uniqcoords[a[1]][0] not in chains:
                uniqcoords[a[1]] = (ch,allcoords[ch][a])

    #set of all unique PDB coordinates in the chains of interest
    seqcoords = [(ch[0],coord) for coord,ch in uniqcoords.items() if ch[0] in chains]

    #construct FASTA from unique coordinates
    seqpdb = SeqRecord(Seq("".join(ch[1] for coord,ch in uniqcoords.items() if ch[0] in chains)),id=pdbid+":"+chain)
    #read pdb sequence from header and construct SeqRecord
    seqpdb.annotations['seqres'] = next(SeqIO.parse(pdb_file_name,"pdb-seqres"))
    seqpdb.annotations['uniprotid'] = seqpdb.annotations['seqres'].dbxrefs[0][4:]
    if len(seqpdb) < len(uniqcoords):
        print("WARNING: Crystal structure not fully covered. Additional chains may need to be selected.",file=sys.stderr)

    #pick to either use the header FASTA as the reference, or to fetch the reference from UniProtKB
    #fetching takes longer, but ensures that the FASTA sequence is standard
    if isinstance(seqquery,str) and seqquery == "fetch":
        seqquery = next(SeqIO.UniprotIO.UniprotIterator(StringIO(urllib.request.urlopen("http://www.uniprot.org/uniprot/"+seqpdb.annotations['uniprotid']+".xml").read().decode("ascii"))))
    elif seqquery is None:
        seqquery = seqpdb.annotations['seqres']

    if len(uniqcoords) < len(seqquery):
        print("WARNING: Crystal structure does not fully cover query.",file=sys.stderr)

    structure_chains = [a for a in structure_allchains if a.id in chains]
    
    seqpdb.id = pdbid + ":" + chain
    seqpdb.name = seqpdb.id
    seqpdb.annotations['coords'] = seqcoords
    seqpdb.annotations['coords_allchains'] = allcoords
    seqpdb.annotations['coords_uniq'] = uniqcoords
    seqpdb.annotations['name'] = handle.name

    #This causes problems when unpickling
#   seqpdb.annotations['structure'] = structure

    seqpdb.annotations['chain'] = chain
    seqpdb.annotations['coordmap'] = _generate_pdb_fasta_coordmap(seqquery,seqpdb)
    seqquery.annotations['pdbseq'] = seqpdb
    return (structure,structure_chains,seqquery)


def _generate_pdb_fasta_coordmap(seqquery,seqpdb,gapopen=13,gapextension=1):
    """Generate a coordinate map between the query FASTA and the PDB FASTA"""
    pdbcoords = seqpdb.annotations['coords']
   
    #generate a gapped seqpdb to guide the pwa later
    seqpdb_mapped = ""
    last = 0
    for c,s in zip(pdbcoords,str(seqpdb.seq)):
        if c[1][1] >= 0: #skip everything before initiator methionine, which sometimes has index 0
            seqpdb_mapped += (c[1][1]-last-1)*"-" + s
            last = c[1][1]
    
    if len(seqpdb_mapped) < len(seqquery):
        seqpdb_mapped += (len(seqquery) - len(seqpdb_mapped))*"-"

    #add penalties for gaps in query sequences
    matrix.update(dict((('-',b),gapextension) for b in set(a for key in matrix.keys() for a in key)))
    matrix.update(dict(((b,'-'),gapextension) for b in set(a for key in matrix.keys() for a in key)))
    #pairwise align the two since you don't trust the PDB coordinate system
    pwa = pairwise2.align.globalds(str(seqquery.seq),seqpdb_mapped,matrix,-gapopen,-gapextension)[0]
    
    #generate map from PDB coords to query coords
    coordmap_ptoq = dict()
    #generate map from query coords to PDB coords
    coordmap_qtop = dict()
    qpos = -1 #position in query fasta
    ppos = -1
    try:
        for i in range(len(pwa[0])):
            if pwa[1][i] == "-" and pwa[0][i] == "-":
                continue
            if pwa[1][i] != "-" and pwa[0][i] != "-":
                #matched
                ppos += 1
                qpos += 1
                coordmap_ptoq[pdbcoords[ppos]] = (qpos,pwa[0][i] == pwa[1][i],ppos)
                coordmap_qtop[qpos] = (pdbcoords[ppos],pwa[0][i] == pwa[1][i],ppos)
                assert seqquery[qpos] == pwa[0][i] and seqpdb[ppos] == pwa[1][i]
            elif pwa[1][i] == "-":
                #gap in PDB
                qpos += 1
                coordmap_qtop[qpos] = (pdbcoords[ppos],pwa[0][i] == pwa[1][i],ppos)
                assert seqquery[qpos] == pwa[0][i]
            else:
                #gap in query
                ppos += 1
                coordmap_ptoq[pdbcoords[ppos]] = (qpos,pwa[0][i] == pwa[1][i],ppos)
                assert seqpdb[ppos] == pwa[1][i]
    except:
        raise Exception("Mismatch while generating the query to PDB coordinate map. This should not happen. Please report this as a bug")
    return coordmap_ptoq,coordmap_qtop,pwa

def _annotate(seqquery,structure,structure_chains,exposed_cutoff=0.25,compute_hse=False,dsspbin="mkdssp",dsspfile=None,**kwargs):
    """Annotate secondary structure and surface exposure. If semethod is set to all, then compute dssp and hse"""
    seqpdb = seqquery.annotations['pdbseq']
    qtop = seqpdb.annotations['coordmap'][1]
    chains = seqpdb.annotations['chain'].split(",")
    
    #initialise to no annotation
    ssmap = dict((i," ") for i,a in enumerate(seqquery))
    semap_dssp = dict((i,np.nan) for i,a in enumerate(seqquery))

    #structure calculators, only define a generator for HSE
    if dsspfile:
        dssp = DSSP(structure,dsspfile,file_type="DSSP",dssp=dsspbin,**kwargs)
    else:
        dssp = DSSP(structure,seqpdb.annotations['name'],dssp=dsspbin,**kwargs)
    hse = (HSExposureCA(a) for a in structure_chains)

    #actually compute HSE if desired
    if compute_hse:
        hse = list(hse) 
        mxCN = max(sum(a[1][:2]) for b in hse for a in b.property_list)
        semap_hse = copy.copy(semap_dssp)

    for i,a in enumerate(seqquery):
        if i in qtop and qtop[i][1] and qtop[i][0][0] in chains:
            if qtop[i][0] in dssp:
                cur = dssp[qtop[i][0]]
                try:
                    assert a == cur[1]
                except:
                    raise Exception("Sequence mismatch. This should not happen. Please report this as a bug.")
                if cur[2] == "-":
                    cur = (cur[0],cur[1]," ",cur[3])
                ssmap[i] = cur[2]
                semap_dssp[i] = cur[3]
            #check if user actually wanted to compute hse
            if compute_hse:
                for h in hse:
                    if qtop[i][0] in h:
                        semap_hse[i] = h[qtop[i][0]]
                        break

    seqquery.letter_annotations["secondary_structure"] = "".join(a[1] for a in sorted(ssmap.items()))
    seqquery.letter_annotations["surface_exposure_dssp"] = np.fromiter((a[1] for a in sorted(semap_dssp.items())),dtype=float,count=len(seqquery))

    if compute_hse:
        seqquery.letter_annotations["surface_exposure_hse_at"] = np.fromiter((np.nan if not isinstance(a[1],tuple) else a[1][0] for a in sorted(semap_hse.items())),dtype=float,count=len(seqquery))
        seqquery.letter_annotations["surface_exposure_hse_ab"] = np.fromiter((np.nan if not isinstance(a[1],tuple) else a[1][1] for a in sorted(semap_hse.items())),dtype=float,count=len(seqquery))
        #seqquery.letter_annotations["surface_exposure"] = "".join(" " if a == -1 else "b" if 1-float(a)/float(mxCN) < exposed_cutoff else " " for a in seqquery.letter_annotations["surface_exposure_hse_at"])
   
    #binary surface exposure calculation 
    seqquery.letter_annotations["surface_exposure"] = "".join(" " if a >= exposed_cutoff else "b" for a in seqquery.letter_annotations["surface_exposure_dssp"])
    
    #compute correlations if predictions computed
    if 'relative_solvent_accessibility_predicted' in seqquery.letter_annotations:
        keys = set((a[0],a[1][0]) for a in qtop.items() if a[1][0] in dssp)
        if compute_hse:
            keys = keys.intersection((a[0],a[1][0]) for a in qtop.items() for h in hse if a[1][0] in h)
        keys = OrderedDict(keys)
        sevalues = np.empty((3,len(keys)),dtype=float)
        sevalues[0] = [a for i,a in enumerate(seqquery.letter_annotations['relative_solvent_accessibility_predicted']) if i in keys]
        sevalues[1] = [dssp[keys[a]][3] for a in keys]
        #FUTURE WORK: distance correlation of secondary structure
        if compute_hse:
            sevalues[2] = [[sum(h[keys[a]][:1]) for a in keys for h in hse if keys[a] in h]]
            corrmat = np.corrcoef(sevalues)
        else:
            seqquery.annotations['surface_exposure_vectors'] = sevalues[:2]
            corrmat = np.corrcoef(sevalues[:2])

        seqquery.annotations['surface_exposure_correlation'] = corrmat
        print("Correlation between surface exposure predicted by NetSurfP and calculated by DSSP" + (compute_hse * " and HSE"))
        print(corrmat)
           
if __name__ == "__main__":
    exit()
