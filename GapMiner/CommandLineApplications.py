#!/usr/bin/env python3

from Bio.Blast.Applications import NcbideltablastCommandline
from Bio.Align.Applications import ClustalOmegaCommandline
from Bio.Application import ApplicationError
from Profiles import MSA
from hhblits import HHblitsCommandline

def homology_search(search_method,query_file,blast_num_alignments,blast_out,localblast=False,db="\"swissprot\"",db_path="/data/BlastDB",outfmt="5",evalue_cutoff=0.001,comp_based_stats="0",show_domain_hits=True,threads=1):
    """BLAST search of the query"""
    assert search_method != "deltablast" or ((not localblast) ^ show_domain_hits)
    if search_method == "deltablast":
        cline = NcbideltablastCommandline(query=query_file, db=db, evalue=evalue_cutoff, remote=(not localblast), num_alignments=blast_num_alignments, outfmt=outfmt, out=blast_out+".xml",out_pssm=blast_out+".pssm.chk",out_ascii_pssm=blast_out+".pssm.txt",comp_based_stats=comp_based_stats,show_domain_hits=True,use_sw_tback=True,num_threads=threads,show_gis=False)
        cline(env={'BLASTDB':db_path})
    elif search_method == "hhblits":
        cline = HHblitsCommandline(input=query_file,output=blast_out+".hhr",hhm_output=blast_out+".hhm",qhhm_output=blast_out+".qhhm",a3m_output=blast_out+".a3m",nofilter=False,protein_db=db_path,ncpu=threads)
        cline()
    else:
        raise Exception("Invalid search method. You should not have gone this far, please report this as a bug.")

def msa_external(blast_out_fasta,msa_out,homologs,threads=1,row_weight_method="equal",hmm_input=None,guide_tree=None):
    """MSA the hit sequences with Clustal Omega"""
    params = dict()
    if hmm_input:
        params['hmm_input'] = hmm_input
    if guide_tree:
        params['guidetree_in'] = guide_tree
    clustalomega_cline = ClustalOmegaCommandline(infile=blast_out_fasta, outfile=msa_out, force=True, verbose=True, auto=True, outfmt="fasta", threads=threads,**params)
    clustalomega_cline()
    return MSA(a2m_file=msa_out,row_weight_method=row_weight_method)

if __name__ == "__main__":
    exit()
