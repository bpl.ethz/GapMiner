#!/usr/bin/env python3

from Bio.Application import _Option, _Switch, _Argument, AbstractCommandline

class HHblitsCommandline(AbstractCommandline):
    def __init__(self, cmd="hhblits", **kwargs):
        self.parameters = [ 
            _Option(['-i','input'],"FASTA/A3M/A2M/HHM input file",filename=True,equate=False,is_required=True),  
            _Switch(['-a','secondary_structure'],"Compute secondary structure"),  
            _Switch(['-all','nofilter'],"Do not filter MSA"),  
            _Option(['-o','output'],"HHR output file",filename=True,equate=False,is_required=True),  
            _Option(['-ohhm','hhm_output'],"HHM output file",filename=True,equate=False,),  
            _Option(['-qhhm','qhhm_output'],"Last query HHM output file",filename=True,equate=False,),  
            _Option(['-opsi','psi_output'],"PSI-BLAST output file",filename=True,equate=False,),  
            _Option(['-oa3m','a3m_output'],"A3M output file",filename=True,equate=False,),  
            _Option(['-cpu','ncpu'],"Number of CPUs to use",equate=False,),  
            _Option(['-n','iterations'],"Number of iterations to use",equate=False,),  
            _Option(['-id','mid'],"Maximum pairwise identity to query",equate=False,),  
            _Option(['-diff','diff'],"Minimum number of sequences per MSA block of 50 (a diversity measure)",),  
            _Option(['-cov','cov'],"Minimum coverage of query",equate=False,),  
            _Option(['-qid','qid'],"Minimum identity to query",equate=False,),  
            _Option(['-Z','maxalnsum'],"Maximum number of alignments in the summary hit list",equate=False,),  
            _Option(['-B','maxaln'],"Maxiumum number of alignments in the alignments list",equate=False,),  
            _Option(['-d','protein_db'],"HHBlits protein DB prefix",filename=True,equate=False,is_required=True),  
            ]   
        AbstractCommandline.__init__(self, cmd, **kwargs)

class HHmakeCommandline(AbstractCommandline):
    def __init__(self, cmd="hhmake", **kwargs):
        self.parameters = [ 
            _Option(['-i','input'],"FASTA/A3M/A2M/HHM input file",filename=True,equate=False,is_required=True),  
            _Option(['-o','output'],"HHM output file",filename=True,equate=False,is_required=True),  
            _Option(['-id','mid'],"Maximum pairwise identity to query",equate=False),  
            _Option(['-diff','diff'],"Minimum number of sequences per MSA block of 50 (a diversity measure)",equate=False),  
            _Option(['-cov','cov'],"Minimum coverage of query",equate=False),  
            _Option(['-qid','qid'],"Minimum identity to query",equate=False),  
            ]   
        AbstractCommandline.__init__(self, cmd, **kwargs)

class HHfilterCommandline(AbstractCommandline):
    def __init__(self, cmd="hhfilter", **kwargs):
        self.parameters = [ 
            _Option(['-i','input'],"FASTA/A3M/A2M input file",filename=True,equate=False,is_required=True),  
            _Option(['-o','output'],"A3M output file",filename=True,equate=False,is_required=True),  
            _Option(['-id','mid'],"Maximum pairwise identity to query",equate=False),  
            _Option(['-diff','diff'],"Minimum number of sequences per MSA block of 50 (a diversity measure)",equate=False),  
            _Option(['-cov','cov'],"Minimum coverage of query",equate=False),  
            _Option(['-qid','qid'],"Minimum identity to query",equate=False),  
            ]   
        AbstractCommandline.__init__(self, cmd, **kwargs)


class ReformatCommandline(AbstractCommandline):
    #make sure hhsuite scripts is in PATH and PERL5LIB
    #format: reformat.pl input output
    def __init__(self, cmd="reformat.pl", **kwargs):
        self.parameters = [
            _Argument(["input"],"Input file",filename=True,is_required=True),
            _Argument(["output"],"Output file",filename=True,is_required=True),
            _Option(['-l','maxlen'],"Maximum line length",filename=False,equate=False,is_required=False),  
            ]
        AbstractCommandline.__init__(self, cmd, **kwargs)

if __name__ == "__main__":
    exit()
