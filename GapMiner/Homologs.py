#!/usr/bin/env python3
#Classes for filtering BLAST and HHblits results. Returns lists in a Homologs object
#Homologs parses a list of FASTAs, or a set of pairwise alignments in FASTA format. by expanding gaps, it can generate an MSA

import lxml.etree as ET
import re
from Bio import Entrez
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import itertools
import numpy as np
from io import StringIO
import sys
import Profiles
from glob import glob
from Bio import Phylo
from copy import copy,deepcopy
import ctypes
import os
import pickle

_re_region = re.compile(r'(-+)')
_splitter = re.compile(r'\s+')
_sp_prefix = os.path.dirname(__file__) + "/"

def di(obj_id):
    return ctypes.cast(obj_id, ctypes.py_object).value

def filter_hits(filter_method,results_file,seqquery,mn,mx,min_coverage,blast_num_alignments=-1,email=None,fullalign=False,filter_by_signature=True):
    if filter_method == "blast":
        blast_filter = BlastFilter(seqquery.id,results_file+".xml")
        homologs = blast_filter.filter_alignments(mn,mx,min_coverage,blast_num_alignments=blast_num_alignments,query=seqquery,filter_by_signature=filter_by_signature)
    elif filter_method == "hhblits":
        homologs = Homologs(psa_file=results_file + ".hhr",fmt="hhr",query=seqquery,mn=mn,mx=mx,maxnaln=blast_num_alignments,min_coverage=min_coverage)
    return homologs


def signature(seq):
    """Calculate a signature for the sequence's gap structure"""
    seq_sp = re.sub(_re_region,r'|\1|',seq).strip("|").split('|')
    gap_init = int(seq_sp[0][0]=="-")
    gap_term = -1 if int(seq_sp[-1][-1]=="-") else len(seq_sp)
    return str(gap_init) + "|" + "|".join(str(len(a)) for a in seq_sp[gap_init:gap_term])

class BlastHit:
    '''Extract all information from a hit'''
    def __init__(self,elem,queryid,querylen):
        self.queryid = queryid
        self.querylen = querylen
        self.hit_id = next(elem.iterfind("Hit_id")).text
        self.title = next(elem.iterfind("Hit_def")).text
        self.accession = next(elem.iterfind("Hit_accession")).text
        self._hitlen = int(next(elem.iterfind("Hit_len")).text)
        id_split = self.hit_id.split("|")
        if id_split[0] == "gnl":
            #DB headers are incorrect, so extract from def
            if self.title.find("|") > -1:
                #of the form 'db|ID[|stuff] <description>'
                self.db,self.accession = self.title.split("|")[:2]
                self.hit_id = "|".join([self.db,self.accession])
                self.title = self.title.split(" ",maxsplit=1)[1]
                self.dbid = self.accession
            else:
                #of the form 'id <description>'
                self.db = ""
                self.hit_id,self.title = self.title.split(" ",maxsplit=1)
                self.dbid = self.hit_id
            self.gi = ""
        elif id_split[0] == "gi":
            #DB headers are correct
            self.gi,self.db,self.dbid = id_split[1:4]
        else:
            self.db,self.dbid = id_split[:2]
            self.gi = ""

        #Extract all HSPs
        self.hsps = (BlastHSP(hsp=a,hit=self) for a in next(elem.iterfind("Hit_hsps")).iterchildren())


class BlastHSP:
    '''Container for storing a HSP from a hhblits or BLAST search'''
    def __init__(self,hsp=None,hit=None,hhblits_hit=None,hitid=None,hitfullid=None,ladd=None,radd=None,hhblits_homolog_table=None,hhblits_psa_stats=None,query_id=None):
        if hsp is not None and hit is not None:
            #Copy over info from hit
            self.title = hit.title
            self.db = hit.db
            self.dbid = hit.dbid
            self._queryid = hit.queryid
            self.hitlen = hit.querylen
    
            #Extract info from HSP
            self.bitscore = float(next(hsp.iterfind("Hsp_bit-score")).text)
            self.score = int(next(hsp.iterfind("Hsp_score")).text)
            self.evalue = float(next(hsp.iterfind("Hsp_evalue")).text)
            self.queryfrom = int(next(hsp.iterfind("Hsp_query-from")).text)
            self.queryto = int(next(hsp.iterfind("Hsp_query-to")).text)
            self.hitfrom = int(next(hsp.iterfind("Hsp_hit-from")).text)
            self.hitto = int(next(hsp.iterfind("Hsp_hit-to")).text)
            self._length = self.hitto - self.hitfrom + 1
            self.identities = next(hsp.iterfind("Hsp_identity")).text
            self.positive = next(hsp.iterfind("Hsp_positive")).text
            self.gaps = int(next(hsp.iterfind("Hsp_gaps")).text)
            self.alignlen = int(next(hsp.iterfind("Hsp_align-len")).text)
            self.sbjct = next(hsp.iterfind("Hsp_hseq")).text
            self.query = next(hsp.iterfind("Hsp_qseq")).text
            self.mid = next(hsp.iterfind("Hsp_midline")).text
            
        elif hhblits_hit is not None and hitid is not None and hitfullid is not None and ladd is not None and radd is not None and hhblits_homolog_table is not None and hhblits_psa_stats is not None:
            #0: query sequence
            #1: query consensus
            #2: midline
            #3: template consensus
            #4: template sequence
            #5: [confidence]
            self.title = hitfullid
            self.db,self.dbid = hitid.split("|",maxsplit=1)
            self._queryid = hit._queryid
            self.hitlen = hit._length

            self.bitscore = hhblits_psa_stats[6]
            self.score = hhblits_psa_stats[2]
            self.evalue = hhblits_psa_stats[1]
            self.queryfrom = hhblits_homolog_table[6]
            self.queryto = hhblits_homolog_table[7]
            self.hitfrom = hhblits_homolog_table[8]
            self.hitto = hhblits_homolog_table[9]
            self._length = self.hitto - self.hitfrom + 1
            self.identities = self._length * hhblits_psa_stats[4]
            #FUTURE: convert similarity from this format to percentage (it is defined as the arithmetic mean of the bitscores)
            self.positive = hhblits_psa_stats[5]
            self.alignlen = len(hhblits_hit[0])
            self.sbjct = hhblits_hit[4]
            self.query = hhblits_hit[0]
            self.gaps = self.sbjct.count('-') + self.query.count('-')
            self.mid = hhblits_hit[2]
        else:
            raise Exception("Some input is missing")

        #Percent identity and similarity
        self.pidentities = float(self.identities) / float(self.hitlen)
        self.ppositives = float(self.positive) / float(self.hitlen)
    
        #how much of the query is covered by the HSP
        self.coverage = float(self._length) / float(self.hitlen)
        
        self.hasgap = self.sbjct.find("-")>-1 or self.query.find("-")>-1

    def seq(self,out_description=False):
        """Return the sequence of the HSP as a SeqRecord"""
        return SeqRecord(Seq((self.queryfrom-1)*"-" + self.sbjct + (self.hitlen-self.queryto)*"-"),description=(self.title if out_description else ""),id=self.db+"|"+self.dbid)
    def seq_fasta(self):
        """Return the sequence of the HSP in FASTA format"""
        return self.seq().format("fasta")

    def qury(self,out_description=False):
        """Return the sequence of the search query as a SeqRecord"""
        return SeqRecord(Seq((self.queryfrom-1)*"-" + self.query + (self.hitlen-self.queryto)*"-"),description="",id=self._queryid)
    def qury_fasta(self):
        """Return the sequence of the search query in FASTA format"""
        return self.qury().format("fasta")

    def info(self):
        #return prob, evalue, pvalue,score,SS,matched columns,query coords,template coords
        return np.array([np.nan,self.evalue,np.nan,self.score,np.nan,self.alignlen,self.queryfrom,self.queryto,self.hitfrom,self.hitto,np.nan],dtype=float)

    
    def __eq__(self,other):
        #HSPs are equal if matching sequences of hit and query identical
        return self.sbjct == other.sbjct and self.query == other.query
    def __len__(self):
        return self._length

class BlastFilter:
    def __init__(self,queryid,blast_out):
        self._alignments = []
        self._blast_out = blast_out
        self.filtered_hsps = []
        self.queryid = queryid
        self.querylen = None
        self._minsims = [1.0]
        self._ranonce = False

    def filter_alignments(self,mn,mx,min_coverage,filter_by_signature=False,query=None,blast_num_alignments=-1,read_all=False):
        """Generate the filtered hits, return as homologs object"""
        blast_outer = XMLParser(self._blast_out,["Hit","BlastOutput_query-len"])
        
        #set of all hit signatures
        t = set()
        homologs = Homologs(None,fmt="empty")
        #Vector defined as: chosensums[a] := the number of chosen hits up until hit a
        homologs._chosensums = []
        homologs._hsps = []
        homologs._homolog_table = []
        homologs._psa_stats = []
        if query:
            assert not isinstance(query,str)
            homologs.query = np.fromiter(iter(str(query.seq)),dtype="S1",count=len(query.seq))
            homologs._queryid = query.id
        try:
            #get query length
            blast_outer_iter = blast_outer.iterator()
            self.querylen = int(next(blast_outer_iter).text)
            homologs._length = self.querylen
            homologs._maxins = np.zeros(homologs._length)
            for elem in blast_outer_iter:
                aln = BlastHit(elem,self.queryid,self.querylen)
                for i,hsp in enumerate(aln.hsps):
                    added = not filter_by_signature
                    #only select HSPs that are at least 80% of the query length
                    if i == blast_num_alignments and not read_all:
                        #stop early
                        break
                    if i < blast_num_alignments and hsp.coverage > min_coverage:
                        #FUTURE: replace identity with similarity?
                        #Only select hits with at least one gap in their HSP
                        if hsp.hasgap and mn <= hsp.pidentities <= mx:
                            if filter_by_signature:
                                #only select hits with unique gap signatures
                                aln_sig = signature(hsp.query)
                                if aln_sig not in t:
                                    t.add(aln_sig)
                                    added = True
                            if added:
                                homologs._hsps.append(hsp)
                                hspinfo = hsp.info()
                                homologs._homolog_table.append(hspinfo)
                                homologs._psa_stats.append(np.concatenate([hspinfo[[0,1,3,5]],[hsp.pidentities,np.nan,np.nan]]))
                                homologs._hit_ids.append(hsp.dbid)
                                homologs._hit_fullids.append(hsp.title)
                                homologs._detect_gaps(np.fromiter(iter(hsp.query),dtype="S1",count=len(hsp.query)),np.fromiter(iter(hsp.sbjct),dtype="S1",count=len(hsp.sbjct)),hsp.queryfrom-1,hsp.hitlen-hsp.queryto)
                    if not self._ranonce:
                        self._minsims.append(min(hsp.pidentities,self._minsims[-1]))
                    homologs._chosensums.append(len(homologs._hsps))
        except ET.XMLSyntaxError:
            raise Exception("The BLAST XML file is truncated. Rerun BLAST.")

        #when converting from lists to numpy matrices, check to make sure lists are nonempty
        homologs._nhomos = len(homologs._hsps)
        homologs._homolog_table = np.vstack(homologs._homolog_table) if len(homologs._homolog_table) else np.empty(shape=(0,11),dtype=float)
        homologs._psa_stats = np.vstack(homologs._psa_stats) if len(homologs._psa_stats) else np.empty(shape=(0,7),dtype=float)
        homologs._psa = np.vstack(homologs._psa) if len(homologs._psa) else np.empty(shape=(0,self.querylen),dtype="S1")
        self._ranonce = True
        return homologs

#used to parse PSAs and convert them to MSAs
class Homologs:
    def __init__(self,psa_file=None,fmt="empty",query=None,mn=None,mx=None,maxnaln=-1,min_coverage=None):
        self._hit_ids = []
        self._hit_fullids = []
        self._psa = []
        self._psagaps = []
        self._psadels = []
        self._nhomos = None
        self._maxins = None
        self._homolog_table = None
        self.query = None
        self._length = None
        self._queryid = None
        self._hsps = None
        self._mn = mn
        self._mx = mx
        self._mincoverage = min_coverage
        self._maxnaln = maxnaln
        if fmt != "empty":
            assert psa_file
            if query is not None:
                if isinstance(query,str):
                    self.query = np.fromiter(iter(query),dtype="S1",count=len(query))
                else:
                    self.query = np.fromiter(iter(str(query.seq)),dtype="S1",count=len(query))
                    self._queryid = query.id
            else:
                print("WARNING: Query sequence not provided, inferring from file. Gaps may be present in query",file=sys.stderr)
            
            assert fmt == "hhr"
            self._parse_hhr(psa_file)
            assert self._nhomos is not None
            assert self._maxins is not None
            assert self._homolog_table is not None
            assert self.query is not None
            assert self._length is not None
            assert self._queryid is not None
            assert self._hsps is not None
            self._psa = np.vstack(self._psa) if len(self._psa) else np.full(shape=(0,self._length),fill_value=b' ',dtype="S1")

    def tomsa(self,row_weight_method="equal"):
        """Convert PSA to MSA by expanding gaps"""
        #Inserts are represented by lowercase letter in hits and '.' in query, deletions by '-'
        assert self.query is not None
        assert len(self._psa)
        msa = np.full((len(self._psa)+1,self._length + np.sum(self._maxins)),b'.',dtype="S1")
        offset = 0
        for i,curins in enumerate(self._maxins):
            msa[0:,i+offset] = self.query[i]
            msa[1:,i+offset] = self._psa[:,i]
            if curins:
                for j,curgaps in enumerate(self._psagaps):
                    if i in curgaps:
                        assert all(a==b'.' for a in msa[j+1,(i+1+offset):(i+1+offset+len(curgaps[i]))])
                        msa[j+1,(i+1+offset):(i+1+offset+len(curgaps[i]))] = np.chararray.lower(curgaps[i])
            offset += curins
        return Profiles.MSA(msa,row_weight_method=row_weight_method,headers=[self._queryid]+self._hit_ids)

    def export_alignments(self,blast_out_fasta,email=None,fullalign=False):
        """Export filtered hits to FASTA file"""
        assert not(fullalign and email is None)
        if email:
            Entrez.email = email
        with open(blast_out_fasta,"w") as fasta_out:
            if fullalign:
                #retrieve full sequences
                fetched = Entrez.efetch(db="protein", rettype="fasta", retmode="text", id=[self._queryid]+self._hit_ids)
                fetched_write = fetched.read()
                fetched.close()
            else:
                #Use sequences of HSPs
                fetched_write = SeqRecord(Seq(self.query.tostring().decode()),id=self._queryid,description="").format("fasta")+"".join(hsp.seq_fasta() for hsp in self._hsps)
            #write result sequences to FASTA file
            fasta_out.write(fetched_write)


    def _detect_gaps(self,query,hit,ladd,radd):
        #detects gaps in two numpy arrays, then appends to the PSA
        #psagaps and psadels elements are lists of dictionaries mapping query coordinates to indel arrays
        for seq,other,storage in [(hit,query,self._psadels),(query,hit,self._psagaps)]:
            binencoding = np.fromiter(itertools.chain((1 for a in range(ladd)), (int(a != b'-') for a in seq), (1 for a in range(radd))),dtype=int)
            curg = np.ma.flatnotmasked_contiguous(np.ma.masked_values(binencoding,1))
            if curg:
                if not isinstance(curg,list):
                    curg = [curg]
                starts = np.fromiter((a.start for a in curg),dtype=int,count=len(curg))
                lens = np.fromiter((a.stop-a.start for a in curg),dtype=int,count=len(curg))
                startinds = starts - np.pad(np.cumsum(lens),(1,0),mode="constant")[:-1]-1
                gapseqs = [other[gap] for gap in curg]
                storage.append(dict(zip(startinds,gapseqs)))
            else:
                lens = []
                startinds = []
                storage.append(dict())
        #generate a list of coordinates corresponding to the query
        if len(lens):
            curgaps = np.zeros(self._length)
            curgaps[startinds] = lens
            #update maximum gap size per position
            self._maxins = np.maximum(curgaps,self._maxins)
            psa_cols_slices = np.ma.flatnotmasked_contiguous(np.ma.masked_values(binencoding,0))
            assert psa_cols_slices is not None
            #find indels, update max insert length for each position, return indices of query columns
            if not isinstance(psa_cols_slices,list):
                psa_cols_slices = [psa_cols_slices]
            query_slice = np.fromiter((b for a in psa_cols_slices for b in range(a.start,a.stop)),dtype=int,count=self._length)
        else:
            query_slice = np.arange(self._length)
        #start building matrix of alignments. inserts are deleted
        self._psa.append(np.concatenate([np.full(ladd,b'-',dtype="S1"),hit,np.full(radd,b'-',dtype="S1")])[query_slice])

    def _parse_hhr(self,psa_file):
        #discard header
        with open(psa_file,"r") as pfile:
            for line in pfile:
                sline = line.strip().split(" ")
                if sline[0] == "Match_columns":
                    self._length = int(sline[1])
                elif sline[0] == "Query":
                    self._queryid = line.strip()[14:].split(" ",maxsplit=1)[0]
                elif sline[0] == "No":
                    break
            #since the homolog table at the top does not list percent identity, need to read the whole table first, then pop out stuff as the alignments are read
            #read homolog list
            self._homolog_table = []
            for i,line in enumerate(pfile):
                if line.strip() == "":
                    break
                sline = _splitter.split(line[35:].strip())
                sline = [float(a) for a in sline[:6]] + [float(a) for a in sline[6].split("-")] + sline[7:]
                if len(sline) < 10:
                    lastsplit = sline[8][:-1].split("(")
                    assert len(lastsplit) == 2
                    sline[8] = lastsplit[0]
                    sline.append(float(lastsplit[1]))
                else:
                    sline[9] = float(sline[9][1:-1])
                intersplit = sline[8].split("-")
                sline[8] = float(intersplit[0])
                sline.append(sline[9])
                sline[9] = float(intersplit[1])
                self._hit_ids.append(line[:35].lstrip().split(" ")[1])
                self._homolog_table.append(sline)
            self._nhomos = len(self._homolog_table)
            #pick first sequence (highest scoring) as best match to query
            topick = 0
            #read pairwise alignments
            line_buffer = [next(pfile)]
            self._psa_stats = np.empty((self._nhomos,7),dtype=float)
            self._maxins = np.zeros(self._length)
            self._hsps = []
            skip = 0
            for i,line in enumerate(pfile):
                if line[:3] == "No " or line[:5] == "Done!":
                    #process buffer
                    #HHR format specification:
                    #line[ 0]:hit number
                    #line[ 1]:full hit id 
                    #line[ 2]:stats
                    #line[ 3]:space
                    #line[ 4+8k]:query seq
                    #line[ 5+8k]:query concensus
                    #line[ 6+8k]:midline
                    #line[ 7+8k]:template concensus
                    #line[ 8+8k]:template seq
                    #line[ 9+8k]:confidence
                    #line[10+8k]:space
                    #line[11+8k]:space
                    #if Confidence is not reported, then 7k instead of 8k
                    j = int(line_buffer[0].strip().split(" ")[-1]) - 1 - skip
                    stats = (a.split("=")[-1] for a in _splitter.split(line_buffer[2].strip()))
                    self._psa_stats[j] = [float(a) if a[-1] != "%" else float(a[:-1])/100.0 for a in stats]
                    if self._psa_stats[j,4] < self._mn or self._psa_stats[j,4] > self._mx or self._psa_stats[j,3]/self._length <= self._mincoverage:
                        self._hit_ids.pop(j)
                        self._homolog_table.pop(j)
                        self._nhomos -= 1
                        topick -= 1
                        skip += 1
                    elif j >= self._maxnaln:
                        self._hit_ids = self._hit_ids[:j]
                        self._homolog_table = self._homolog_table[:j]
                        self._nhomos = self._maxnaln
                        break
                    else:
                        self._hit_fullids.append(line_buffer[1][1:])
                        query_start = int(_splitter.split(line_buffer[4])[2])
                        hit_start   = int(_splitter.split(line_buffer[8])[2])
                        assert query_start == self._homolog_table[j][6]
                        assert   hit_start == self._homolog_table[j][8]
                       
                        #if Confidence is reported, read in groups of 8, otherwise 7 
                        ssize = 7 + (line_buffer[9][:10] == "Confidence")
                        curhit = [str() for a in range(ssize-2)]
    
                        psa = [[a[3] if len(a) > 3 else a[1] for a in (_splitter.split(a) for a in lines[:6])] for lines in itertools.zip_longest(*[iter(line_buffer[4:])]*ssize)]
                        
                        #fold multi-line alignments into one
                        for cline in psa[:-1]:
                            curhit = [a + b for a,b in zip(curhit,cline)]

                        lastsplits = []
                        lastlines = []
                        lastinds = []
                        for b in line_buffer[-ssize:-2]:
                            lastsplits.append(_splitter.split(b))
                            lastlines.append(lastsplits[-1][3] if len(lastsplits[-1]) > 3 else lastsplits[-1][1])
                        curhit = [a + b for a,b in zip(curhit,lastlines)]

                        ladd = query_start - 1
                        radd = self._length - int(lastsplits[0][4])
                        lpad = ladd*"-"
                        rpad = radd*"-"
                        #0: query sequence
                        #1: query consensus
                        #2: midline
                        #3: template consensus
                        #4: template sequence
                        #5: [confidence]
    
                        #ENCODING:
                        #store everything as the same length as the query. inserts are in a separate data structure
    
                        #add gaps to correct lengths and construct numpy arrays
                        if self.query is None and j == topick:
                            self.query = np.fromiter(iter(lpad + curhit[0].replace("-","") + rpad),dtype="S1",count=self._length)
                            #halts if the resulting string is too short
                            assert self._length == len(self.query)
                        psa = [np.fromiter(iter(lpad + a + rpad),dtype="S1") for a in curhit]
                        self._hsps.append(BlastHSP(hit=self,hhblits_hit=curhit,hitid=self._hit_ids[j],hitfullid=self._hit_fullids[j],ladd=ladd,radd=radd,hhblits_homolog_table=self._homolog_table[j],hhblits_psa_stats=self._psa_stats[j,:]))
                        #consensus and confidence are being read then discarded
    
                        #lotsa sanity checks
                        assert len(curhit[0]) == len(curhit[4])
                        assert len(psa[0].tostring().decode().replace("-","")) == self._homolog_table[j][7]-self._homolog_table[j][6]+1
                        assert len(psa[4].tostring().decode().replace("-","")) == self._homolog_table[j][9]-self._homolog_table[j][8]+1
                        assert self._homolog_table[j][7]-self._homolog_table[j][6]+1+ladd+radd == self._length
                        assert self._psa_stats[j,3] == self._homolog_table[j][5]
    
                        self._detect_gaps(psa[0][ladd:len(psa[0])-radd],psa[4][ladd:len(psa[4])-radd],ladd,radd)

                    #reset buffer
                    line_buffer = [line]
                else:
                    line_buffer.append(line)
            self._homolog_table = np.vstack(self._homolog_table) if len(self._homolog_table) else np.empty(shape=(0,11),dtype=float)


    def __len__(self):
        """Length of the query"""
        return self._length
    def size(self):
        """Number of homologs"""
        return self._nhomos

class XMLParser:
    """A XML parser that doesn't hog ALL the memory. Implements Liza's fast_iter from http://www.ibm.com/developerworks/xml/library/x-hiperfparse/"""
    def __init__(self,xmlfile,tags):
        self._xmlfile = xmlfile
        self._tags = tags

    def iterator(self,reset=True):
        if reset:
            if hasattr(self,'_parser'):
                del self._parser
            self._parser = ET.iterparse(self._xmlfile)
        elif not hasattr(self,'_parser'):
            raise Exception("Parser not initialised. Set 'reset' flag to True")
        for event,elem in self._parser:
            if elem.tag in self._tags:
                yield elem
#            elem.clear()
#            for ancestor in elem.xpath('ancestor-or-self::*'):
#                while ancestor.getprevious() is not None:
#                    del ancestor.getparent()[0]
        del self._parser




if __name__ == "__main__":
    print("Foo")

