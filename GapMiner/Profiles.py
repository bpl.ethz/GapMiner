#!/usr/bin/env python3

#MSA parses an A2M file, detects gaps, and generates a indel length distro
#HMM parses an HHM, or can convert an MSA into an HMM. it also computes indel and transition probabilities, and sequence variability

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import Bio.SubsMat.MatrixInfo
import numpy as np
import itertools
import re
import scipy.stats
import scipy.misc
from collections import Counter,OrderedDict
import sys
import Homologs
from copy import copy,deepcopy
from scipy.sparse.csgraph import johnson
from scipy.sparse import dok_matrix
from Bio import AlignIO
from Bio.Phylo.TreeConstruction import DistanceCalculator

from hhblits import HHmakeCommandline,HHfilterCommandline

_splitter = re.compile(r'\s+')

#log probability of "0", used to avoid -inf
_base_log_prob = -99.999 * np.log(2)



#labels from PSSM
_labels = ["A","R","N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V"]
_label_selector = np.fromiter((d for c,d in sorted((a,i) for i,a in enumerate(_labels))),dtype=int)

#residue background frequencies
_res_probs_log_raw = np.array([-3.706, -5.728, -4.211, -4.064, -4.839, -3.729, -4.763, -4.308, -4.069, -3.323, -5.509, -4.64 , -4.464, -4.937, -4.285, -4.423, -3.815, -3.783, -6.325, -4.665]) * np.log(2)
#normalise to sum to 1
_res_probs_log = _res_probs_log_raw - scipy.misc.logsumexp(_res_probs_log_raw)
_var_measure_default = "jssd"

#the diagonal along a substitution matrix
_smat_default = Bio.SubsMat.MatrixInfo.blosum62
_smatdiag_dict = dict((key[0],float(val)) for key,val in _smat_default.items() if key[0]==key[1])
_smatdiag = np.fromiter((_smatdiag_dict[a] / 2 * np.log(2) for a in sorted(_labels)),dtype=float) + _res_probs_log
#make sure everything is a probability
assert np.all(_smatdiag <= 0)

class MSA:
    def __init__(self,msa=None,a2m_file=None,row_weight_method="equal",headers=None):
        #stores short and 
        self._headers = []

        #either provide a numpy character matrix MSA, or an a2m file
        assert (a2m_file is None) ^ (msa is None)

        if a2m_file:
            #sequence MSA in char format
            self._seqmsa = []
            self._parse_a2m(a2m_file)
        else:
            self._seqmsa = msa 
            self._headers = headers
        self._seq = SeqRecord(Seq(self._seqmsa[0].tostring().decode().replace(".","")))
        self._length = len(self._seq)
        assert len(self._headers) == len(self._seqmsa)

        #compute row weights
        self._compute_row_weights(method=row_weight_method,count_gaps=True,ignore_identical=False)
        #find inserts and compute insertion and deletion length distributions
        self._compute_length_dist()

    def _indel_dist(self):
        return copy(self._length_dist_all)

    def len_dist_entropy(self):
        base = scipy.stats.entropy(self._indel_dist().T)
        base[base == 0] = np.exp(_base_log_prob)
        return base

    def evidence(self):
        return copy(self._evidence)

    def ins_len_dist_entropy(self):
        base = scipy.stats.entropy(np.exp(self._length_dist).transpose())
        base[base == 0] = np.exp(_base_log_prob)
        return base

    def del_len_dist_entropy(self):
        base = scipy.stats.entropy(np.exp(self._length_dist_del).transpose())
        base[base == 0] = np.exp(_base_log_prob)
        return base

    def length_dist(self):
        return np.exp(self._length_dist)

    def toa2m(self):
        return "\n".join(">"+head+"\n"+seq.tostring().decode() for head,seq in zip(self._headers,self._seqmsa))+"\n"

    def headers(self):
        return copy(self._headers)

    def gap_slices(self):
        #Check to make sure gaps have been computed
        assert self._binmsa_slices_corrected is not None
        assert len(self._bindmsa_slices_merged) == len(self._del_pos)
        return {'insertions':(self._binmsa_slices_corrected,self._length_dist[self._ins_pos,:]),'deletions':(self._bindmsa_slices_merged,self._length_dist_del[self._del_pos,:])}
    def match_slices(self):
        return self._match_slices

    def __getitem__(self,slices):
        return self._seqmsa[slices]
    def __len__(self):
        #should be length of query sequence without gaps
        return self._length
    def size(self):
        #number of columns in the MSA
        return self._seqmsa.shape[1]
    
    def profile_size(self):
        return self._seqmsa.shape[0]
    
    def _parse_a2m(self,a2m_file):
        with open(a2m_file,"r") as fa2m:
            self._a2m = AlignIO.read(fa2m,"fasta")
        self._headers = OrderedDict()
        self._seqmsa = np.empty((len(self._a2m),self._a2m.get_alignment_length()),dtype="S1")
        scoring_matrix = DistanceCalculator('blosum62').scoring_matrix
        dists = []
        for i,a in enumerate(self._a2m):
            name = a.description
            if name in self._headers:
                start = 1
                name = name.split(" ")
                name[0] += "_0"
                name = " ".join(name)
                while name in self._headers:
                    name = name.split(" ")
                    name[0] = "_".join(name[0].split("_")[:-1])+"_"+str(start)
                    name = " ".join(name) 
                    start += 1
                a.description = name
                a.id = name.split(" ")[0]
            self._headers[name] = None
            self._seqmsa[i] = list(str(a.seq))
            a.seq = Seq(str(a.seq).replace(".","-").upper())
            #adapted from Bio.Phylo.TreeConstruction
            dist = 0.0
            max_dist1 = 0.0
            max_dist2 = 0.0
            for q,h in zip(str(self._a2m[0].seq),str(a.seq)):
                if q != "-" and h != "-":
                    #the distance calculator in TreeConstructor doesn't do gap penalties
                    qfail = False
                    hfail = False
                    try:
                        max_dist1 += scoring_matrix[q,q]
                    except ValueError:
                        max_dist1 += scoring_matrix["X","X"]
                        qfail = True
                    try:
                        max_dist2 += scoring_matrix[h,h]
                    except ValueError:
                        max_dist2 += scoring_matrix["X","X"]
                        hfail = True
                    dist += scoring_matrix[q if not qfail else "X",h if not hfail else "X"]
            max_dist = max(max_dist1,max_dist2)
            dists.append(1.0 - (float(dist) / float(max_dist)) if max_dist else 1.0)
        self._headers = list(self._headers.keys())
        #do some cleanup (add '.' and lowercase for inserts)
        binmsa = (self._seqmsa != b'-').astype(int)
        binmsa_slices = np.ma.flatnotmasked_contiguous(np.ma.masked_values(binmsa[0],1))
        if binmsa_slices:
            if not isinstance(binmsa_slices,list):
                binmsa_slices = [binmsa_slices]
            for sl in binmsa_slices:
                self._seqmsa[:,sl] = np.chararray.replace(np.chararray.lower(self._seqmsa[:,sl]),b'-',b'.')
        #calculate distance matrix
        self._distmat = np.array(dists,dtype=float)

    def _compute_row_weights(self,method="equal",count_gaps=True,ignore_identical=False):
        if method == "equal":
            self._row_weights = np.full(self._seqmsa.shape[0],1.0/float(self._seqmsa.shape[0]),dtype=float)
        elif method in ["henikoff","distance"]:
            #assign Henikoff position weights, same shape as MSA
            self._henikoff_weights2d = np.zeros(shape=self._seqmsa.shape,dtype=float)
            for i in range(self._seqmsa.shape[1]):
                curcol = self._seqmsa[:,i].tostring().decode().upper()
                if not count_gaps:
                    curcol.replace("-","").replace(".","")
                counts = Counter(curcol)
                if len(counts) > 1 or not ignore_identical:
                    for key in counts:
                        self._henikoff_weights2d[self._seqmsa[:,i] == key.encode(),i] = 1.0/float(counts[key])
                    self._henikoff_weights2d[:,i] /= np.sum(self._henikoff_weights2d[:,i])
            #fold down weights to vector
            self._row_weights = np.sum(self._henikoff_weights2d,axis=1)
            if method == "distance":
                self._row_weights *= (1-self._distmat) / np.sum(1-self._distmat)
            self._row_weights /= np.sum(self._row_weights)
        else:
            raise Exception('Method should be one of "equal", "henikoff", or "distance"')

    def _compute_length_dist(self):
        #determines insert sites and generates an insert length distribution
        self._binmsa = (self._seqmsa != b'.').astype(int)
        self._match_slices = np.ma.flatnotmasked_contiguous(np.ma.masked_values(self._binmsa[0],0))
        if not isinstance(self._match_slices,list):
            self._match_slices = [self._match_slices]
        #the list of inserts
        self._binmsa_slices = np.ma.flatnotmasked_contiguous(np.ma.masked_values(self._binmsa[0],1))
        if self._binmsa_slices is None:
            self._binmsa_slices = []
        if not isinstance(self._binmsa_slices,list):
            self._binmsa_slices = [self._binmsa_slices]
        #calculate insert positions
        starts = np.fromiter((a.start for a in self._binmsa_slices),dtype=int,count=len(self._binmsa_slices))
        lens = np.fromiter((a.stop-a.start for a in self._binmsa_slices),dtype=int,count=len(self._binmsa_slices))
        maxlen = np.max(lens) if len(lens) else 0

        #compute insert length counts
        #histo: rows are the slice indices, columns are the insert lengths
        #does not store the count of no inserts
        histo = np.full((len(self._binmsa_slices),maxlen),np.exp(_base_log_prob),dtype=float)
        inslens = []
        for i,sl in enumerate(self._binmsa_slices):
            #takes a length distribution and a histogram matrix and puts the weighted length distro in the histo
            #i is a slice (a gap), while the columns are the indel lengths
            #only compute insert lengths on the hits (reduces the 0-length count by 1)
            inslens.append(np.sum(self._binmsa[:,sl],axis=1))
            bins = np.sort(list(set(inslens[-1])))
            histo[i,bins[1:]-1] = curhist = [b if b > 0 else np.exp(_base_log_prob) for b in (np.sum(self._row_weights[inslens[-1] == a]) for a in bins[1:])]
        #rows: indices of slices, cols: indices of homologs
        used_rows = np.zeros(shape=(self._binmsa.shape[0],self._length),dtype=bool)
        self._length_dist,self._ins_pos = self._length_dist_from_histo(histo,starts,lens)
        self._binmsa_slices_corrected = [slice(a,a+1) if a > -1 else slice(0,0) for a in self._ins_pos]
        for i,a in enumerate(self._binmsa_slices_corrected):
            if a.start:
                used_rows[np.where(inslens[i] > 0)[0],a.start] = 1
        
        #find deletions
        goodsel = np.ma.flatnotmasked_contiguous(np.ma.masked_values(self._binmsa[0],0))
        goodsel = goodsel if isinstance(goodsel,list) else [goodsel]
        bin_shrunk = np.hstack([self._seqmsa[:,sl] for sl in goodsel])
        assert bin_shrunk.shape[1] == self._length
        
        #rows are number of homologs, columns are the sequence indices
        self._bindmsa = (bin_shrunk != b'-').astype(int)
        self._bindmsa_slices = [np.ma.flatnotmasked_contiguous(np.ma.masked_values(a,1)) for a in self._bindmsa]

        #histo: rows are sequence indices, columns are deletion lengths
        histo = np.full((self._length,self._length),np.exp(_base_log_prob),dtype=float)
        maxdel = np.zeros(self._length,dtype=int)
        extra_weight = 0
        for i,d in enumerate(self._bindmsa_slices):
            if d:
                if not isinstance(d,list):
                    d = [d]
                sts = np.fromiter((sl.start for sl in d),dtype=int,count=len(d))
                lns = np.fromiter((sl.stop-sl.start for sl in d),dtype=int,count=len(d))
                maxdel[sts] = np.maximum(maxdel[sts],lns)
                if np.any(used_rows[i,sts]):
                    extra_weight += self._row_weights[i]
                histo[sts,lns-1] += self._row_weights[i]

        #Use the step=1 to indicate a deletion. step=None is an insert
        self._bindmsa_slices_merged = [slice(i,i+a,1) for i,a in enumerate(maxdel) if a>0]
        starts = np.fromiter((a.start for a in self._bindmsa_slices_merged),dtype=int,count=len(self._bindmsa_slices_merged))

        self._length_dist_del,self._del_pos = self._length_dist_from_histo(histo[starts,:np.max(maxdel)],starts)
        self._length_dist_all = np.exp(np.hstack([self._length_dist,self._length_dist_del])) / (1+extra_weight)
        #correct for double-sampling of some sequences
        self._length_dist_all = np.hstack([1-np.sum(self._length_dist_all,axis=1).reshape(-1,1),self._length_dist_all])
        error_pos = np.where(self._length_dist_all[:,0] < 0)[0]
        for ep in error_pos:
            self._length_dist_all[ep,0] = np.exp(_base_log_prob)
            self._length_dist_all[ep,:] /= np.sum(self._length_dist_all[ep,:])
        assert np.all(self._length_dist_all[:,0] >= 0)
        evidence_f = self._length_dist_all * self._binmsa.shape[0]
        self._evidence = np.round(evidence_f).astype(int)
        diffs = np.sum(self._evidence,axis=1) - self._bindmsa.shape[0]
        self._evidence[:,0] -= diffs
        error_pos = np.where(self._evidence[:,0] < 0)[0]
        if len(error_pos):
            self._evidence[error_pos,0] += diffs[error_pos]
            for ep in error_pos:
                f_error = np.argsort(self._evidence[ep].astype(float)-evidence_f[ep])
                self._evidence[ep,f_error[-diffs[ep]:]]-=1
                assert(np.sum(self._evidence[ep])==self._bindmsa.shape[0])
        assert np.all(self._evidence >= 0)

    def _length_dist_from_histo(self,histo,starts,lens=None):
        #generates logspace length distro for whole sequence
        length_dist = np.log(histo)

        #indel positions
        #if lens is None, then deletions, else insertions, so subtract 1 to get index of preceding residue
        _pos = starts - int(lens is not None) * (1 + np.pad(np.cumsum(lens),(1,0),mode="constant")[:-1])
        
        #per-position insert probabilities. nrows is length of query, ncols is max number of columns in MSA
        #by default, minimum probability from HHblits
        _length_dist = np.full((self._length,histo.shape[1]),_base_log_prob,dtype=float)

        #by default, no inserts
#        _length_dist[:,0] = np.log1p(-(histo.shape[1]-1)*np.exp(_base_log_prob))
       
        #map length_dist to correct rows
#        _length_dist[_pos.astype(int),:] = (length_dist.transpose() - scipy.misc.logsumexp(length_dist,axis=1)).transpose()
        _length_dist[_pos.astype(int),:] = length_dist
        return _length_dist,_pos.astype(int)

 

class Profile:
    def __init__(self,seqquery=None):
        self._desc = ""
        self._null = []
        self._aacid_freqs = []
        self._seqquery = seqquery
    
    def __len__(self):
        return self._length
    def aacid_freqs(self):
        return np.exp(self._aacid_freqs)
    def aacid_freqs_log(self):
        return self._aacid_freqs
    def null(self):
        return np.exp(self._null)
    def null_log(self):
        return self._null
    def seq(self):
        return self._seq
    def _check_seq(self):
        """If a seqquery is provided, check to make sure it matches the inputs"""
        assert not self._seqquery or (self._seq.seq == self._seqquery.seq)


    def conservation(self,measure=_var_measure_default):
        measures = {\
            'kl':self._kl_divergence,
            'js':self._js_divergence,
            'jsd':self._js_distance,
            'jss':self._jss_divergence,
            'jssd':self._jss_distance,
        }
        try:
            assert measure in measures
        except AssertionError:
            raise Exception("Invalid conservation measure. Please pick one of 'kl', 'js', 'jsd', 'jss', 'jssd'")
        return measures[measure]()

    def conservation_site(self,measure=_var_measure_default):
        consfunc = self.conservation(measure=measure)
        return np.hstack([np.sqrt(consfunc[:-1] * consfunc[1:]),consfunc[-1]])

    def _conservation_fit(self,seqvar_fit,measure=_var_measure_default):
        if measure in ["kl","js"] :
            return scipy.stats.gamma(*seqvar_fit)
        elif measure in ["jsd","jssd"]:
            return scipy.stats.beta(*seqvar_fit)
        else:
            raise Exception("Invalid measure chosen. This error should not occur here. Please report this as a bug.")
        return None

    def pconservation(self,seqvar_fit,measure=_var_measure_default):
        """Return probability that the residues are conserved. The distribution of values is fit to a Gamma distribution"""
        #prob of getting a conservation score less than or equal to current. high if highly conserved
        return self._conservation_fit(seqvar_fit,measure).cdf(self.conservation_site(measure=measure))

    def pvariability_log(self,seqvar_fit,measure=_var_measure_default):
        return self._conservation_fit(seqvar_fit,measure).logsf(self.conservation_site(measure=measure))
#        return np.log1p(-self.pconservation(seqvar_fit,measure))

    def swappiness_log(self,seqvar_fit,swap_win_size=1,measure=_var_measure_default):
        return pad_add(self.pvariability_log(seqvar_fit,measure),range(swap_win_size),_base_log_prob)
    
    #transformations of other functions, not used in calculations
    def pconservation_log(self,seqvar_fit,measure=_var_measure_default):
        return self._conservation_fit(seqvar_fit,measure).logcdf(self.conservation_site(measure=measure))
#        logprobs = np.log(self.pconservation(seqvar_fit,measure))
#        #correct for 0 probability
#        logprobs[logprobs == -np.inf] = _base_log_prob
#        return logprobs

    def pvariability(self,seqvar_fit,measure=_var_measure_default):
        return self._conservation_fit(seqvar_fit,measure).sf(self.conservation_site(measure=measure))
#        return np.exp(self.pvariability_log(seqvar_fit,measure))

    def swappiness(self,seqvar_fit,swap_win_size=1,measure=_var_measure_default):
        return np.exp(self.swappiness_log(seqvar_fit,range(swap_win_size),measure))
        

    #since _null and _aacid_freqs are already in log space, faster to compute divergences manually rather than use built-ins
    #done in log2 space by default to ensure that JS divergence is <=1
    #these assume all input distributions are normalised
    def _kl_divergence(self,exp_base=2):
        base = (self._null - self._aacid_freqs)/np.log(exp_base)
        ent = np.dot(base,self.null())
        return ent

    def _js_divergence(self,exp_base=2):
        m = (np.logaddexp(self._aacid_freqs,self._null)-np.log(2))/np.log(exp_base)
        ent = 0.5 * (np.dot(self._null/np.log(exp_base) - m,self.null()) \
                + np.sum(self.aacid_freqs() * (self._aacid_freqs/np.log(exp_base) - m),axis=1) )
        return ent

    def _js_distance(self,exp_base=2):
        return np.sqrt(self._js_divergence(exp_base=exp_base))

    def _jss_divergence(self,exp_base=2,smatdiag=_smatdiag):
        #smatdiag in log space
        m = (np.logaddexp(self._aacid_freqs,self._null)-np.log(2))/np.log(exp_base)
        ent = 0.5 * (np.dot(self._null/np.log(exp_base) - m,np.exp(smatdiag) * self.null()) \
                + np.sum(np.exp(smatdiag) * self.aacid_freqs() * (self._aacid_freqs/np.log(exp_base) - m),axis=1) )
        return ent

    def _jss_distance(self,exp_base=2,smatdiag=_smatdiag):
        return np.sqrt(self._jss_divergence(exp_base,smatdiag))


#DELTA-BLAST PSSM
class PSSM(Profile):
    def __init__(self,pssm_file,res_probs_log=_res_probs_log,seqquery=None):
        super().__init__(seqquery)
        with open(pssm_file,"r") as fpssm:
            lines = [a.rstrip("\n") for a in fpssm][2:-4]
        #lines[0] contains the amino acid order
        self._seq = SeqRecord(Seq("".join(a[6] for a in lines[1:])),description=self._desc)
        self._check_seq()
        self._length = len(self._seq)

        #log space
        self._aacid_freqs = np.vstack([np.fromiter((float(a[i:(i+3)]) for i in range(9,len(a)-1,3)),dtype=float) for a in lines[1:]])[:,_label_selector] / 2 * np.log(2) + _res_probs_log
        #normalise to sum to 1
        self._aacid_freqs = (self._aacid_freqs.transpose() - scipy.misc.logsumexp(self._aacid_freqs,axis=1)).transpose()
        self._null = _res_probs_log
    def __getitem__(self,slices):
        return self._aacid_freqs[slices]

#HHblits HMM
#need A2M and maybe HHR. Will convert A2M to HHM if HHM not given
class HMM(Profile):
    def __init__(self,msa=None,a2m_file=None,hhm_file=None,hhr_file=None,homologs=None,seqquery=None,row_weight_method="henikoff",mn=None,mx=None,cov=None):
        #Provide either an MSA object or an a2m_file, not both
        try:
            assert (msa is None) ^ (a2m_file is None)
        except AssertionError:
            raise Exception("Provide one of an MSA or and a2m_file, not both")
#       try:
#           assert (hhr_file is None) ^ (homologs is None)
#       except AssertionError:
#           raise Exception("Provide one of an HHR file or Homologs object")

        super().__init__(seqquery)
#        self._hits = None
        if msa:
            self._msa = msa
        else:
            self._msa = MSA(a2m_file=a2m_file,row_weight_method=row_weight_method)
        self._profile_size = self._msa.profile_size()
        self._evidence = self._msa.evidence()
        self._len_dist_entropy = self._msa.len_dist_entropy()
        self._ins_len_dist_entropy = self._msa.ins_len_dist_entropy()
        self._del_len_dist_entropy = self._msa.del_len_dist_entropy()
#        self._distmat = self._msa._distmat
        
        #read HHM
        if not hhm_file:
            if a2m_file:
                hhm_file = a2m_file[:a2m_file.rfind(".")]+".hhm"
            else:
                raise Exception("Please provide an A2M file if no HHM is available")
            #convert A2M to HMM
            if mn and mx and cov:
                hhm_cline = HHmakeCommandline(input=a2m_file,output=hhm_file,mid=int(mx*100),qid=int(mn*100),cov=int(cov*100))
            else:
                hhm_cline = HHmakeCommandline(input=a2m_file,output=hhm_file,mid=100,qid=0,cov=0,diff=0)
            hhm_cline()
        self._parse_hhm(hhm_file) 

        #checks to see if MSA matches HHM
        try:
            self._check_seq()
            assert self._length == self._msa._length
            assert self._seq.seq == self._msa._seq.seq
        except:
            raise Exception("MSA and HHM mismatch")
        

        #read HHR or Homologs object
        #self._hits = Homologs.Homologs(hhr_file,fmt="hhr",query=seqquery) if hhr_file is not None else homologs

        #fix weird case where Pr[I->M] > 0, but Pr[M->I] = 0
        self._transitions[self._transitions[:,1] == _base_log_prob,3] = _base_log_prob
        #precompute deletion probabilities
        self._del_len_probs_log = np.hstack([\
            (self._transitions[:,2] + np.hstack([self._transitions[1:,5],[_base_log_prob]])).reshape(-1,1),
            np.vstack([self._transitions[:,2] + np.hstack([self._transitions[a:,6],np.full(a,_base_log_prob,dtype=float)]) + np.hstack([self._transitions[(a+1):,5],np.full(a+1,_base_log_prob,dtype=float)]) for a in range(1,self._length)]).T,
            ])
        self._ins_len_probs_log = np.vstack([self._transitions[:,1] + self._transitions[:,4]*a + self._transitions[:,3] for a in range(self._length)]).T
        lims = zip(\
                np.sum(np.cumsum(np.exp(self._ins_len_probs_log),axis=1) > np.exp(self._transitions[:,1]).reshape(-1,1),axis=1),
                np.sum(np.cumsum(np.exp(self._del_len_probs_log),axis=1) > np.exp(self._transitions[:,2]).reshape(-1,1),axis=1)\
                )
        for i,(a,b) in enumerate(lims):
            self._ins_len_probs_log[i,-a:] = _base_log_prob
            self._del_len_probs_log[i,-b:] = _base_log_prob

        #after everything, don't need the MSA anymore
        delattr(self,'_msa')


    def size(self):
        return self._profile_size

    def _parse_hhm(self,hhm_file):
        with open(hhm_file,"r") as fhhm:
            a=0
            self._header = []
            #read header
            for line in fhhm:
                self._header.append(line)
                if line[:5] == "LENG ":
                    #check if hhm profile correct length
                    self._length = int(_splitter.split(line)[1])
                if line[:5] == "NAME ":
                    self._desc = line[6:].strip()
                if line[:4] == "HMM ":
                    self.aacids = _splitter.split(line.rstrip("\n"))[1:21]
                    a = 1
                if line[:5] == "NULL ":
                    self._null = np.fromiter((float(a) for a in _splitter.split(line.rstrip("\n"))[1:21]),dtype=float) / -1000 * np.log(2)
                    #normalise to sum of 1
                    self._null -= scipy.misc.logsumexp(self._null)
                if a:
                    if a==3:
                        break
                    else:
                        a += 1

            self._hmm = np.empty(shape=(self._length,33),dtype=float)
            self._hhm = []
            for i,lines in enumerate(itertools.zip_longest(*[fhhm]*3)):
                if lines[0][:2] == "//":
                    break
                self._hhm.append("".join(lines))
                sline = _splitter.split(" ".join(lines))
                #use ord(b) to convert amino acid sequence to ASCII characters. Convert to bit scores for everything else
                self._hmm[i] = [ord(b) if not i else float(b)/-1000*np.log(2) if b != "*" else _base_log_prob for i,b in enumerate(sline[0:33])]
            self._transitions = self._hmm[:,23:30]
            self._aacid_freqs = self._hmm[:, 2:22]
            self._hmm[:,30:] *= -1000/np.log(2)
        self._seq = SeqRecord(Seq("".join(chr(int(a)) for a in self._hmm[:,0])),description=self._desc)

    def __getitem__(self,slices):
        return self._hmm[slices]

    def __len__(self):
        return self._hmm.shape[0]
        
    def transitions(self):
        #0:M->M, #1: M->I, #2:M->D, #3:I->M, #4:I->I, #5:D->M, #6:D->D
        return np.exp(self._transitions)
    def transitions_log(self):
        return copy(self._transitions)
    def neff(self):
        #exp of the average entropy
        return self._hmm[:,30:]

#    def ins_prob_log(self,n=1,at_least=False,model="length_dist"):
#        if model == "profile":
#            if n == 0:
#                base = np.copy(self._transitions[:,0])
#            #exponential decay insert model
#            else:
#                if n-1 >= self._ins_len_probs_log.shape[1]:
#                    return np.full(self._ins_len_probs_log.shape[0],_base_log_prob,dtype=float)
#                if not at_least:
#                    base = self._ins_len_probs_log[:,n-1]
#                else:
#                    base = scipy.misc.logsumexp(self._ins_len_probs_log[:,(n-1):],axis=1)
#        elif model == "length_dist":
#            #weighted gap length distribution from MSA
#            if n < self._msa._length_dist.shape[1]:
#                base = scipy.misc.logsumexp(self._msa._length_dist[:,n:],axis=1) if at_least else self._msa._length_dist[:,n]
#            else:
#                #default to zero prob
#                base = np.full(self.__len__(),_base_log_prob,dtype=float)
#        else:
#             raise Exception("Invalid insert model. Pick either 'profile' or 'length_dist'")
#        return base
#
#    def ins_prob(self,n=1,at_least=False,model="length_dist"):
#        return np.exp(self.ins_prob_log(n=n,at_least=at_least,model=model))
#
    def len_dist_entropy(self,model="length_dist"):
        if model == "length_dist":
            return self._len_dist_entropy
        else:
            base = scipy.stats.entropy(np.exp(np.hstack([self._del_len_probs_log,self._ins_len_probs_log])).transpose())
            base[base == 0] = np.exp(_base_log_prob)
            return base
            #return -np.sum(self._transitions[:,:3] / np.log(2) * np.exp(self._transitions[:,:3]),axis=1)

    def ins_len_dist_entropy(self):
        return self._ins_len_dist_entropy
    
    def del_len_dist_entropy(self):
        return self._del_len_dist_entropy
    
#    def del_prob_log(self,n=1,at_least=False):
#        if n == 0:
#            base = np.copy(self._transitions[:,0])
#        else:
#            if n-1 >= self._del_len_probs_log.shape[1]:
#                return np.full(self._del_len_probs_log.shape[0],_base_log_prob,dtype=float)
#            if not at_least:
#                base = self._del_len_probs_log[:,n-1]
#            else:
#                base = scipy.misc.logsumexp(self._del_len_probs_log[:,(n-1):],axis=1)
#
#        return base
#
#    def del_prob(self,n=1,at_least=False):
#        return np.exp(self.del_prob_log(n=n,at_least=at_least))
#
#    def msa(self):
#        return self._msa

    def seq(self):
        return self._seq

    def evidence(self):
        return self._evidence

def pad_add(vector,rnge,fill=0):
    rnge = iter(rnge)
    if type(fill) in [int,float,np.float64]:
        fill = (fill,)
    return np.sum((np.pad(vector[x:],(0,x),mode="constant",constant_values=fill) for x in rnge),axis=0)

if __name__ == "__main__":
    exit()
