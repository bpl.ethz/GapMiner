#!/usr/bin/env python3
import pickle
import numpy as np
import QueryFetcher
from collections import OrderedDict
from sklearn import linear_model
from sklearn.svm import OneClassSVM
from sklearn.feature_extraction.image import extract_patches
from scipy.stats import gmean, beta
import sys
from copy import copy

feature_labels = [\
    "Sequence variability",
    "Length variability (ilrp-HMM)",
    "Secondary structure",
    "RSA",
    "Length variability (p-HMM)",
    "Evidence (number of hits)",
    "Sequence conservation",
    "Sequence variability (of pHMM if Clustal Omega used)",
    "Length variability (of hhblits ilrp-HMM if Clustal Omega used)",
    "Length variability (of hhblits p-HMM if Clustal Omega used)",
    "Sequence conservation (of pHMM if Clustal Omega used)",
    "Labels",
]

def window_smooth(a,width=5):
    return np.mean(extract_patches(np.concatenate([a,np.full(width-1,a[-1],dtype=a.dtype)]),width),axis=1)

def window_smooth_geom(a,width=5):
    return gmean(extract_patches(np.concatenate([a,np.full(width-1,a[-1],dtype=a.dtype)]),width),axis=1)


def get_scores(seqquery,fitted_parameters,conservation_measure="jssd",swap_win_size=1):
    #fitted distribution parameters
    ss_distance_decay = fitted_parameters['ss_distance_decay']
    rsa_fit           = fitted_parameters['rsa_fit']
    seqvar_fit        = fitted_parameters['seqvar_fit']

    hmm = seqquery.annotations['hmm']
    evidence = hmm.evidence()

    score_sources      = np.full((len(seqquery),12),np.NaN,dtype=float)

    score_sources[:,0] = 1-beta.cdf(hmm.conservation_site('jssd'), *seqvar_fit)
    score_sources[:,1] = hmm.len_dist_entropy()
    score_sources[:,2] = QueryFetcher.score_secondary_structure(seqquery,ms=ss_distance_decay)
    score_sources[:,3] = beta.cdf(window_smooth_geom(seqquery.letter_annotations['relative_solvent_accessibility_predicted'],2),*rsa_fit)
    score_sources[:,4] = hmm.len_dist_entropy(model="profile")
    score_sources[:,5] = np.sum(hmm.evidence()[:,1:],axis=1)
    score_sources[:,6] = hmm.conservation_site('jssd')
    score_sources[:,7] = 1-beta.cdf(seqquery.annotations['hmm_profile'].conservation_site('jssd'), *seqvar_fit)
    score_sources[:,8] = seqquery.annotations['hmm_profile'].len_dist_entropy()
    score_sources[:,9] = seqquery.annotations['hmm_profile'].len_dist_entropy(model="profile")
    score_sources[:,10] = seqquery.annotations['hmm_profile'].conservation_site('jssd')
    score_sources[:,11] = annotations_to_labels(seqquery)

    seqquery.annotations['feature_labels'] = feature_labels

    return score_sources

def annotations_to_labels(seqquery):
    binannot,region_names = QueryFetcher.annotate_sites_bin(seqquery)
    annotation_labels = (~np.sum(binannot,axis=0).astype(bool)).astype(int)
    annotation_labels[annotation_labels == 1] = -1
    return annotation_labels

def load_params(fitted_parameter_file="FittedParameters.p"):
    with open(fitted_parameter_file,"rb") as pifile:
        fitted_parameters = pickle.load(pifile)
    return fitted_parameters

def predict_proba(feat_matrix, classifier, fitted_parameters):
    #seqvar_fit        = fitted_parameters['seqvar_fit']

    #conservation = copy(feat_matrix[:,0])
    #conservation = np.concatenate([np.sqrt(conservation[:-1] * conservation[1:]),[conservation[-1]]])
    #variability = 1-beta.cdf(conservation, *seqvar_fit)

    #rsa = copy(feat_matrix[:,3])

    #feat_matrix_transf = np.hstack([variability.reshape(-1,1), feat_matrix[:,1:4]])
    return classifier.predict_proba(feat_matrix)[:,1] 

if __name__ == "__main__":
    exit()
