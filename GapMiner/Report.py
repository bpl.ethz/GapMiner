#!/usr/bin/env python3

import numpy as np
import json
from copy import copy
import time
from scipy.stats import gmean

_jsonEncoder = json.JSONEncoder()

def makeProtDict(seqquery,tagcutoff=0.5):
    features = copy(seqquery.annotations['scores'])
#    features[:,:4] = np.exp(features[:,:4])

    datadict = {
        "uniprotid": seqquery.id,
        "name":      seqquery.annotations['recommendedName_fullName'],
        "organism":  seqquery.annotations['organism'],
        "seq":       str(seqquery.seq), 
        "len":       int(len(seqquery)),
        "secstruct": seqquery.letter_annotations['secondary_structure_predicted'],
        "annots":    "",
        "shortname": seqquery.annotations['gene_name_primary'] if 'gene_name_primary' in seqquery.annotations \
                else seqquery.annotations['gene_name_ordered locus'][0] if 'gene_name_ordered locus' in seqquery.annotations \
                else seqquery.annotations['gene_name_synonym'][0] if 'gene_name_synonym' in seqquery.annotations \
                else "NA",
        "genenames": seqquery.annotations['gene_name_ordered locus'] if 'gene_name_ordered locus' in seqquery.annotations \
                else seqquery.annotations['gene_name_synonym'] if 'gene_name_synonym' in seqquery.annotations \
                else "NA",
        "tagcutoff": round(tagcutoff,2),
        "prob":      seqquery.letter_annotations['taggability'].tolist(),
        "seqvar":    features[:,0].tolist(),
        "lenvar":    features[:,1].tolist(),
        "secstr":    features[:,2].tolist(),
        "rsa":       features[:,3].tolist(),
        "evidence":  features[:,5].tolist(),
        "numhits":   int(seqquery.annotations['hmm'].size()),
        "querymod":  seqquery.annotations['modified'],
        'dbcreated':   time.strftime("%Y-%m-%d"),
        "meanseqvar":gmean((features[:,0]+0.001).tolist())-0.001,
    }
    return datadict

def outputJSON(seqquery,tagcutoff):
    return _jsonEncoder.encode(makeProtDict(seqquery,tagcutoff))



if __name__ == "__main__":
    exit()

