#!/bin/bash
set -e # Stop on error
set -u # Stop on undefined variables

if [[ ! -d "netsurfp-1.0" ]]; then
    while [[ ! -f netsurfp-1.0d.Linux.tar.gz ]]; do
    	echo "Please download the netsurfp 1.0d archive into the current folder and press ENTER once done"
    	xdg-open "http://www.cbs.dtu.dk/cgi-bin/sw_request?netsurfp+1.0"
	    read -p "Press enter to continue"
    done
    # Unpack NetSurfP 1.0d - fail first if getting this dependency was forgotten
    tar -xzvf netsurfp-1.0d.Linux.tar.gz
    
    # Patch netsurfp configuration
    cd netsurfp-1.0
    cp netsurfp netsurfp.bak
    patch netsurfp ../netsurfp.patch
    cd ..
fi

if [[ -d "BlastData" ]]; then
    echo "Dependencies were installed already" 
    exit 0
fi

# Install dependencies
sudo apt install -y python3-pip texlive-latex-recommended texlive-fonts-recommended dvipng texlive-latex-extra
pip3 install --user -r requirements.txt 

# Donwload HH-Suite
wget http://wwwuser.gwdg.de/%7Ecompbiol/data/hhsuite/releases/all/hhsuite-2.0.16-linux-x86_64.tar.gz
tar -xzvf hhsuite-2.0.16-linux-x86_64.tar.gz

# Download BLAST
wget https://ftp.ncbi.nih.gov/blast/executables/legacy.NOTSUPPORTED/2.2.18/blast-2.2.18-x64-linux.tar.gz
tar -xzvf blast-2.2.18-x64-linux.tar.gz

# Install NetSurfP Databases
cd netsurfp-1.0 
wget http://www.cbs.dtu.dk/services/NetSurfP-1.0/data.tar.gz
tar -xzvf data.tar.gz
wget http://www.cbs.dtu.dk/services/NetSurfP-1.0/nr70_db.tar.gz
tar -xzvf nr70_db.tar.gz
cd ..

# Download UniProt DB
wget http://wwwuser.gwdg.de/~compbiol/data/hhsuite/databases/hhsuite_dbs/old-releases/uniprot20_2015_06.tgz
tar -xzvf uniprot20_2015_06.tgz


# Download BLAST DB
wget www.bpl.ethz.ch/software/gapminer/BlastData.tar.gz
tar -xzvf BlastData.tar.gz


# Clean up
rm hhsuite-2.0.16-linux-x86_64.tar.gz
rm blast-2.2.18-x64-linux.tar.gz
rm netsurfp-1.0d.Linux.tar.gz
rm netsurfp-1.0/data.tar.gz
rm netsurfp-1.0/nr70_db.tar.gz
rm uniprot20_2015_06.tgz
rm BlastData.tar.gz