#GapMiner configuration options
#  email: A valid email address.
#  path:  The path to write the results folder. Make sure nothing is overwritten.
#  blastnum: The maximum number of hits for DELTA-BLAST to return. Lower values mean faster runtimes, but fewer distant hits.
#  threads: The number of CPU threads to use for alignment and filtering.
#  minidentity: The minimum fraction of sequence identity for a hit to be considered.
#  maxidentity: The maximum fraction of sequence identity for a hit to be considered.
#  forceblast: Force a new BLAST search, even if one has already been performed.
#  forcemsa: Force a recomputation of the multiple sequence alignment
#  fullalign: MSA using full sequences rather than the HSPs

{\
    'email': '',\
    'path': '.',\
    'blastnum':500,\
    'threads':1,\
    'mincoverage':0.8,\
    'minidentity':0.2,\
    'maxidentity':1,\
    'protein_db':'/home/gapminer/Desktop/GapMiner/GapMiner/netsurfp-1.0/nr70_db/nr70',\
    'search_db_path':'/home/gapminer/Desktop/GapMiner/GapMiner/uniprot20_2015_06/uniprot20_2015_06',\
    'forceblast':False,\
    'remoteblast':False,\
    'forcemsa':False,\
    'fullalign':False,\
    'row_weight_method':'henikoff',\
    'search_method':'hhblits',\
    'force_annot':False,\
    'force_filter':False,\
    'search_db':"uniprot",\
    'msa_method':'hhblits',\
    'substitution_matrix':'blosum62',\
    'debug':False,\
    'netsurfp':'/home/gapminer/Desktop/GapMiner/GapMiner/netsurfp-1.0/netsurfp',\
    'localquery':True,\
}
