#!/usr/bin/env python3

import os.path
import pickle
import sys
import imp
import ast
from optparse import OptionParser,OptionGroup,OptionValueError,SUPPRESS_HELP
import Bio.SubsMat.MatrixInfo
import InitialSetup

def _parse_options():
    """Get options from command line or config file"""
    usage = "Usage: %prog [options] [UniProtKB ID]"
    description = """Predict sites for tag insertion"""
    version = "This version"
    parser = OptionParser(usage=usage, description=description, version=version)
    recalc = OptionGroup(parser, 'Recalculate intermediate steps')
    apaths = OptionGroup(parser, 'Paths and flags for external applications')
    params = OptionGroup(parser, 'Profile construction parameters')

    parser.add_option(      '--setup',               action='store_true', dest='setup',               default=None, help="Initial setup.")
    parser.add_option('-c', '--config',              type="str",          dest="config",              default=None, help='Location of configuration file storing parameters. Passed arguments will override these')
    parser.add_option('-p', '--pdbid',               type="str",          dest="pdbid",               default=None, help='PDB ID and chains. Format: PDBID[:CHAIN1[,CHAIN2[,...]]]. Otherwise, the previously used ID will be used. If none was ever provided, secondary structure will be predicted computationally.')
    parser.add_option(      '--isoform',             type="str",          dest="isoform",             default=None, help=SUPPRESS_HELP) #undocumented features, muahahahaa
    parser.add_option('-P', '--path',                type="str",          dest="path",                default=None, help='Path. Data will be stored in [Path]/[UniProtId] (Default ".")')
    parser.add_option(      '--msa_input',           type="str",          dest="msa_input",           default=None, help='If search_method == "manual", specifies the path to an MSA to use as input instead of searching and filtering.')
    parser.add_option('-n', '--threads',             type='int',          dest="threads",             default=None, help='The number of threads for Clustal Omega to use. (Default 1)')
    parser.add_option(      '--debug',               action='store_true', dest='debug',               default=None, help='Print debugging information to screen.')

    recalc.add_option(      '--force_blast',         action='store_true', dest='force_blast',         default=None, help="Force a recomputation of the BLAST results")
    recalc.add_option(      '--force_hhblits',       action='store_true', dest='force_hhblits',       default=None, help="Force a recomputation of the hhblits results")
    recalc.add_option(      '--force_msa',           action='store_true', dest='force_msa',           default=None, help="Force a recomputation of the MSA")
    recalc.add_option(      '--force_annot',         action='store_true', dest='force_annot',         default=None, help="Force a refetch of the query sequence and annotation")
    recalc.add_option(      '--force_filter',        action='store_true', dest='force_filter',        default=None, help="Force a refiltering of the homology search results")
    recalc.add_option(      '--onlyoutput',          action='store_true', dest='onlyoutput',          default=None, help='Write an output file from an already processed run')
    parser.add_option_group(recalc)

    apaths.add_option(      '--email',               type="str",          dest="email",               default=None, help='Email address for remote (online) NetSurfP and DELTA-BLAST queries')
    apaths.add_option(      '--search_method',       type="str",          dest="search_method",       default=None, help='Homology search method. Can be either "hhblits", "deltablast", or "manual" if a set of homologs is manually provided (Default: "hhblits")')
    apaths.add_option(      '--search_db',           type="str",          dest="search_db",           default=None, help='Space-separated list of protein databases to use for DELTA-BLAST searches. (Default \"uniprot\").')
    apaths.add_option(      '--msa_method',          type="str",          dest="msa_method",          default=None, help='Method used to calculate MSA of similar sequences. If hhblits is used as the search method, can be either "hhblits" or "msa" to use Clustal Omega. This option is ignored if search method is set to "deltablast".')
    apaths.add_option(      '--netsurfp',            type="str",          dest="netsurfp",            default=None, help='Path to NetSurfP. If set to "remote", uses the remote server. (Default "remote")')
    apaths.add_option(      '--protein_db',          type="str",          dest="protein_db",          default=None, help='Prefix of the BLAST DB to use for NetSurfP if running locally.')
    apaths.add_option(      '--search_db_path',      type="str",          dest="search_db_path",      default=None, help='hhblits: prefix of the protein database. DELTA-BLAST: directory of protein and conserved domain databases.')
    apaths.add_option(      '--localblast',          action='store_true', dest='localblast',          default=None, help="Run BLAST searches from a local DB instead of using the NCBI server.")
    apaths.add_option(      '--localquery',          action='store_true', dest="localquery",          default=None, help='Use a local copy of UniProtKB for fetching queries. Set this option if you are running GapMiner for multiple queries to prevent being blocked.')
    parser.add_option_group(apaths)

    params.add_option(      '--blastnum',            type='int',          dest="blastnum",            default=None, help="The number of results blast returns (Maximum 20000)")
    params.add_option(      '--row_weight_method',   type="str",          dest="row_weight_method",   default=None, help='Sequence weighting method to use (Default "helikoff"')
    params.add_option(      '--mincoverage',         type='float',        dest="mincoverage",         default=None, help='The lower cutoff for coverage of the query sequence length. (Default 0.8)')
    params.add_option(      '--minidentity',         type='float',        dest="minidentity",         default=None, help='The lower cutoff for sequence identity to the query. (Default 0.4)')
    params.add_option(      '--maxidentity',         type='float',        dest="maxidentity",         default=None, help='The upper cutoff for sequence identity to the query. (Default 1)')
    params.add_option(      '--substitution_matrix', type="str",          dest="substitution_matrix", default=None, help='Substitution matrix to use for protein-protein alignments and feature functions (Default "blosum62"). See the documentation of the BioPython Bio.SubsMat.MatrixInfo module for a list of valid matrices.')
    params.add_option(      '--fullalign',           action='store_true', dest='fullalign',           default=None, help="Fetch the full sequences for all hits when aligning instead of using just the HSP")
    parser.add_option_group(params)


    #Default parameters
    defaults = {
        'netsurfp':            'remote',
        'search_db':           'uniprot',
        'search_method':       'hhblits',
        'blastnum':            500,
        'row_weight_method':   'henikoff',
        'mincoverage':         0.8,
        'minidentity':         0.2,
        'maxidentity':         1.0,
        'threads':             1,
        'substitution_matrix': 'blosum62',
        'force_blast':         False,
        'force_hhblits':       False,
        'force_msa':           False,
        'force_annot':         False,
        'force_filter':        False,
        'localblast':          False,
        'localquery':          False,
        'fullalign':           False,
        'onlyoutput':          False,
        'debug':               False,
    }

    (options, args) = parser.parse_args()
    options = vars(options)
    opts = dict()
    mats = Bio.SubsMat.MatrixInfo.available_matrices
  
    if options['setup']:
        print("Constructing Uniprot local database")
        InitialSetup.setup_uniprot_db(args[1:])
        print("Done")
        exit()

    #check if UniProt ID provided 
    if len(args):
        opts['query_id'] = args[0]
    else:
        raise OptionValueError("Please provide a valid UniProtKB ID")

    
    #import settings from file
    if options['config']:
        if options['config'][-3:] != ".py":
            options['config'] = options['config'] + ".py"
        #ast.literal_eval only evaluates code that leads to the creation of basic types
        with open(options['config'],"r") as config_file:
            options_cfg = ast.literal_eval(config_file.read())
        assert isinstance(options_cfg,dict)
    else:
        options_cfg = dict()

    #SET SETTINGS, preferring command line arguments, then config, then default
    opts.update(dict(\
             (opt,pval)             if  pval     is not None \
        else (opt,options_cfg[opt]) if  opt      in options_cfg \
        else (opt,defaults[opt])    if  opt      in defaults \
        else (opt,None)             for opt,pval in options.items() \
    ))

    #CHECKS for invalid parameters
    if opts['search_method'] == "deltablast" and opts['msa_method'] == "hhblits":
        print('WARNING: "hhblits" MSA method can only be used when "search_method" == "hhblits". Setting "msa_method" = "msa"',file=sys.stderr)
        opts['msa_method'] = "msa"

    if opts['blastnum'] > 20000 and opts['search_method'] == "deltablast" and not opts['localblast']:
        print("WARNING: Blast can only return at most 20000 hits. The number of hits has been reduced to 20000",file=sys.stderr)
        opts['blastnum'] = 20000

    if opts['email'] is None and opts['search_method'] == "deltablast" and (not opts['localblast'] or opts['fullalign']):
        raise OptionValueError("You must provide a valid email address")
        
    if opts['path'] is None:
        if input('WARNING: No output path given. Write files to the current directory? Y/n') in ['y','Y','']:
            opts['path'] = "."
        else:
            raise OptionValueError("No path to store files given. I can't make that decision for you, I'm just a program...")

    if opts['netsurfp'] != "remote" and opts['protein_db'] is None:
        raise OptionValueError("NetSurfP set to run locally, but no BLAST DB prefix given.")

    if opts['search_db']:
        opts['search_db'] = "\"" + opts['search_db'] + "\""

    if opts['substitution_matrix'] in mats:
        opts['substitution_matrix'] = vars(Bio.SubsMat.MatrixInfo)[opts['substitution_matrix']]
    else:
        raise OptionValueError("Invalid substitution matrix, pick one of " + str(mats))

    if opts['search_method'] == "manual":
        if opts['msa_input'] is None or not os.path.isfile(opts['msa_input']):
            raise OptionValueError("Please provide a valid MSA file")
        opts['search_db'] = "manual"

    if opts['search_method'] not in ["deltablast","hhblits","manual","profile"]:
        raise OptionValueError("Homology search method "+opts['search_method']+" not supported")

    #Make directory for the files
    pwd = opts['path'].rstrip("/") + "/"
    if pwd[:2] == "./":
        pwd = pwd[2:]
    directory = pwd + opts['query_id']
    if not os.path.exists(directory):
        os.makedirs(directory)
    opts['directory'] = directory
    opts['prefix'] = directory + "/" + opts['query_id']
    opts['filter_method'] = "hhblits" if opts['search_method'] in ["hhblits"] else "blast"
    #Search results file
    opts['blast_out'] = opts['prefix'] + (".hhblits" if opts['search_method'] == "hhblits" \
                                     else ".blast"   if opts['search_method'] == "deltablast" \
                                     else "") + ("." + opts['search_db'].replace("\"","").replace(" ","_") if opts["search_method"] == "deltablast" else "")
    print("Writing search results to",opts['blast_out'])

    #filtered hits file
    opts['filtered_out'] = opts['blast_out'] + "." + str(opts['minidentity'])
    print("Writing filtered results to",opts['filtered_out'])

    #alignment file
    opts['msa_out'] = opts['filtered_out'] + "." + opts['msa_method']
    print("Writing MSA to",opts['msa_out'])

    #pickle file
    opts['gaps_pickle_out'] = opts['msa_out'] + ".p"

    #checks if local UniProtKB database has been generated, if applicable
    if opts['localquery']:
        opts['xml_db'] = os.path.dirname(__file__) + "/" + "UniprotSeqRecords.p"
        if not os.path.isfile(opts['xml_db']):
            raise OptionValueError("Local UniProtKB database not generated. Either set localquery to False or run gapminer --setup")
    else:
        opts['xml_db'] = None



    if opts['onlyoutput']:
        with open(opts['gaps_pickle_out'], "rb") as dump:
            seqquery = pickle.load(dump)
        if 'pdbid' not in opts and 'pdbseq' in seqquery.annotations:
            opts['pdbid'] = seqquery.annotations['pdbseq'].id
        if 'hmm' not in seqquery.annotations:
            raise OptionValueError("Complete GapMiner data not found")
        opts['seqquery'] = seqquery

    return opts
    

