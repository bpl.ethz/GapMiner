File descriptions
-----------------
[ClassifierTraining.py]
- Code used to extract data and train the binary classifiers. If the regenerate variable is set to True, then it recomputes everything from the files in the JSON and Runs folders after running gapminer. If set to False, it loads everything from the pickle files in Precomputed

[Figures.py]
- Generates the ML result figures and computes classifier evaluation scores. If regenerate is set to True, it loads training results from the current directory. If set to False, it loads them from Precomputed/

[input_ids]
- A list of protein IDs used
[json_list]
- A list of JSON files to use
[lit_data.txt]
- Table of literature data used as training labels

Contents of precomputed:
[lengths.p]
- Lengths of each protein in input_ids
[results.p]
- Results of binary classifier training
[features.p]
- The feature matrix
[rsa_raw.p]
- Relative surface area predictions used to calculate relative surface accessibility
[conservation.p]
- Sequence conservation scores used to calculate sequence variability
[annots.p]
- Annotations for each residue. -1 means NA, 0 means untaggable, 1 means taggable
