#!/usr/bin/env python3
import pickle
import scipy.stats
import sys 
import numpy as np
import sklearn
from collections import OrderedDict
from sklearn.utils import resample
from sklearn.tree import DecisionTreeClassifier
import matplotlib as mpl 
mpl.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib import rc
import sklearn.metrics
from sklearn.grid_search import GridSearchCV
import sklearn.cross_validation
from copy import copy
from matplotlib.backends.backend_pdf import PdfPages
sys.path.append("../GapMiner")
import forest
import json
import time
import os.path

rc('font',**{'family':'sans-serif','serif':['Palatino'],'sans-serif':['Computer Modern Sans serif']})
rc('text', usetex=True)

def fonts_inc(ax,factor=2.0):
    #http://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(item.get_fontsize()*factor)



#################################
#LOAD FEATURES

#####IF SET TO TRUE, EXTRACT ANNOTAITONS FROM PICKE FILES
regenerate = False

#list of all UniProt IDs, computed files, and JSONS
ids = np.array([a.rstrip() for a in open("input_ids","r")])
computed = ["Runs/"+a+"/"+a+".hhblits.0.2.hhblits.p" for a in ids]
jsons = [a.rstrip() for a in open("json_list","r")]

if regenerate:
    prefix = ""
else:
    prefix = "Precomputed/"

#LOAD DATA
with open(prefix+"features.p","rb") as pfile:
    features = pickle.load(pfile)

with open(prefix+"lengths.p","rb") as pfile:
    lengths = pickle.load(pfile)

clengths = np.cumsum(lengths)
def get_residue(prot,x=None):
    idx = np.where(ids==prot)[0][0]
    start = clengths[idx-1]
    return np.arange(start,start+lengths[idx]) if x is None else start - 1 + x

#LOAD DB LABELS AND OTHER VARIABLES
with open(prefix+"annots.p","rb") as afile:
    newannots = pickle.load(afile)

with open(prefix+"rsa_raw.p","rb") as afile:
    rsa_raw = pickle.load(afile)

with open(prefix+"conservation.p","rb") as afile:
    conservation = pickle.load(afile)
    


#################################
#SET UP TRAINING AND TEST SETS
test_size = 0.2

pos_split = 0.8
random_state=0


#######SET NUMBER OF THREADS HERE
n_jobs = 8
verbosity = 10
n_estimators = 2000


res_per_prot=float(features.shape[0])/float(lengths.size)
pos_thres=3
fp_thres=1

def precision_recall_balanced_curve(estimator,X,y):
    desc_func = estimator.decision_function(X) if hasattr(estimator,'decision_function') else estimator.predict_proba(X)[:,1].reshape(-1,1) if hasattr(estimator,'predict_proba') else X
    adjust = 1/10
    weights = y.shape[0] / (2*np.bincount(y.astype(int))) * adjust
    sample_weight = weights[y.astype(int)]
    return sklearn.metrics.precision_recall_curve(y,desc_func,sample_weight=sample_weight)

def precision_recall_balanced_threshold(estimator,X,y,res_per_prot=res_per_prot,pos_thres=pos_thres,fp_thres=fp_thres):
    precision,recall,prthres = precision_recall_balanced_curve(estimator,X,y)
    return prthres[np.where((precision >= res_per_prot/pos_thres*recall*np.sum(y==1)/y.shape[0]/precision-float(fp_thres)/float(pos_thres))[:-1])[0][0]]

def precision_recall_balanced(estimator,X,y,reverse=False):
    precision,recall,prthres = precision_recall_balanced_curve(estimator,X,y)
    score = sklearn.metrics.auc(recall[::-1],precision[::-1])
    return 1-score if reverse else score


#################################
#LOAD ML RESULTS PICKLE

with open(prefix+"results.p","rb") as dfile:
    results = pickle.load(dfile)

bc_names = [
    "BRF",
    "BRF (with pHMM)",
    "DTC",
    "DTC (with pHMM)",
]

brf = results['brf']
brf_phmm = results['brf_phmm']
dtc = results['dtc']
dtc_phmm = results['dtc_phmm']
bcs = [brf,brf_phmm,dtc,dtc_phmm]

best_param = results['best_param']
best_param_phmm = results['best_param_phmm']
best_param_dtc = results['best_param_dtc']
best_param_dtc_phmm = results['best_param_dtc_phmm']
best_params = [best_param,best_param_phmm, best_param_dtc,best_param_dtc_phmm]

finaltruth = results['finaltruth']
finaltest = results['finaltest']

#################################
#RESULTS

print("","Results")

#GINI IMPORTANCE
for bc,bcname,bcparam in zip(bcs,bc_names,best_params):
    print(bcname)
    print(
        "Gini importances\n",
        "mean",
        np.mean(np.vstack([a.feature_importances_ for a in (bc.estimators_ if hasattr(bc,'estimators_') else [bc])]),axis=0),
        "std",
        np.std(np.vstack([a.feature_importances_ for a in (bc.estimators_ if hasattr(bc,'estimators_') else [bc])]),axis=0),
        "\nvalidation score",
        bcparam.mean_validation_score,
    )

print()

#################################
#FIGURES

blue = (0,0,1,0.5)
red  = (1,0,0,0.5)

#Draw ROC curve
def draw_roc(outfile, X,y,test,selects,bcs,labels):
    f,ax = plt.subplots(figsize=(7,7))
    ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
    for select,bc,label in zip(selects,bcs,labels):
        curX = X[test,:][:,select]
        if bc is not None:
            curX = bc.predict_proba(curX)[:,1]
        print("AUROC", label,sklearn.metrics.roc_auc_score(y,curX))
        ax.plot(*sklearn.metrics.roc_curve(y,curX)[:2],label=label)

    plt.legend(loc="lower right")
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    fonts_inc(ax,2.0)
    pp=PdfPages(outfile)
    pp.savefig(f)
    pp.close()
    plt.show()
    print()

#Draw WPR curve
def draw_wpr(outfile, X, y, test, selects, bcs, labels):
    f,ax = plt.subplots(figsize=(7,7))
    plt.axhline(y=0.5, ls="--", c=".3")
    for select,bc,label in zip(selects,bcs,labels):
        print("WUWPR",label,precision_recall_balanced(bc, X[test,:][:,select], y))
        ax.plot(*precision_recall_balanced_curve(bc, X[test,:][:,select], y)[:2][::-1],label=label)
    
    plt.legend(loc="lower left")
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    fonts_inc(ax,2.0)
    pp=PdfPages(outfile)
    pp.savefig(f)
    pp.close()
    plt.show()
    print()

#ilrpHMM vs pHMM ROC and WPR
draw_roc("Figures/phmm_vs_ilrphmm_roc.pdf", features, finaltruth, finaltest, [[1],[4]], [None, None], ["ilrpHMM", "pHMM"])
draw_wpr("Figures/phmm_vs_ilrphmm_wpr.pdf", features, finaltruth, finaltest, [[1],[4]], [None, None], ["ilrpHMM", "pHMM"])

#ROC and WPR for each feature
draw_roc("Figures/feature_roc.pdf", features, finaltruth, finaltest, [[0],[1],[2],[3]], [None, None, None, None], ["Sequence variability", "Length variability", "Preservation of secondary structure", "RSA"])
draw_wpr("Figures/feature_wpr.pdf", features, finaltruth, finaltest, [[0],[1],[2],[3]], [None, None, None, None], ["Sequence variability", "Length variability", "Preservation of secondary structure", "RSA"])

#ROC and WPR for predictors
draw_roc("Figures/predictors_roc.pdf", features, finaltruth, finaltest, [[0,1,2,3],[0,4,2,3],[0,1,2,3],[0,4,2,3]], bcs, bc_names)
draw_wpr("Figures/predictors_wpr.pdf", features, finaltruth, finaltest, [[0,1,2,3],[0,4,2,3],[0,1,2,3],[0,4,2,3]], bcs, bc_names)


#SUPPLEMENTARY
#sec structs
f,ax = plt.subplots(figsize=(8,6))
plt.xlim([0.3,1])
plt.yscale("log",nonposy="clip")
plt.hist(features[:,2],bins=50,color=blue,weights=np.zeros(features.shape[0])+1./features.shape[0])
plt.xlabel("Probability of preserving nearest non-coil")
plt.ylabel("Fraction residues (log scale)")
pp=PdfPages("Figures/secstruct.pdf")
pp.savefig(f)
pp.close()
plt.show()

