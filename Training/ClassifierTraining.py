#!/usr/bin/env python3
import pickle
import scipy.stats
import sys 
import numpy as np
import sklearn
from collections import OrderedDict
from sklearn.utils import resample
from sklearn.tree import DecisionTreeClassifier
import matplotlib as mpl 
mpl.use('Agg')
import matplotlib.pyplot as plt 
from matplotlib import rc
import sklearn.metrics
from sklearn.grid_search import GridSearchCV
import sklearn.cross_validation
from copy import copy
from matplotlib.backends.backend_pdf import PdfPages
sys.path.append("../GapMiner")
import forest
import json
import time
import os.path


#################################
#LOAD FEATURES

#####IF SET TO TRUE, EXTRACT ANNOTAITONS FROM PICKE FILES
regenerate = False

#list of all UniProt IDs, computed files, and JSONS
ids = np.array([a.rstrip() for a in open("input_ids","r")])
computed = ["Runs/"+a+"/"+a+".hhblits.0.2.hhblits.p" for a in ids]
jsons = [a.rstrip() for a in open("json_list","r")]

#LOAD DATA
if regenerate:
    jsondec = json.JSONDecoder()
    preds_json = OrderedDict((cid,jsondec.decode(open(cjson).read())) for cid,cjson in zip(ids,jsons))
    feature_list = []
    length_list = []
    for idx,cjson in preds_json.items():
        feature_list.append(np.hstack([
            np.array(cjson['seqvar']).reshape(-1,1),
            np.array(cjson['lenvar']).reshape(-1,1),
            np.array(cjson['secstr']).reshape(-1,1),
            np.array(cjson['rsa']   ).reshape(-1,1),
        ]))
        length_list.append(int(cjson['len']))

    features = np.vstack(feature_list)
    lengths = np.array(length_list,dtype=int)
    with open("features.p","wb") as pfile:
        pickle.dump(features, pfile)
    with open("lengths.p","wb") as pfile:
        pickle.dump(lengths, pfile)


else:
    with open("Precomputed/features.p","rb") as pfile:
        features = pickle.load(pfile)
    
    with open("Precomputed/lengths.p","rb") as pfile:
        lengths = pickle.load(pfile)

clengths = np.cumsum(lengths)
def get_residue(prot,x=None):
    idx = np.where(ids==prot)[0][0]
    start = clengths[idx-1]
    return np.arange(start,start+lengths[idx]) if x is None else start - 1 + x

#################################
#LOAD LITERATURE DATA
raw_file = "lit_data.txt"

with open(raw_file,"r") as rfile:
    raw_data = [a.rstrip("\n").split("\t")[:-1] for a in rfile][1:]

#propagate positive labels
pospm = 1
#map each ID to a pair representing positive and negative cases
lit_data = OrderedDict()
for a in raw_data:
    if a[0] not in lit_data:
        lit_data[a[0]] = [OrderedDict(),OrderedDict()]
    if len(a[2]):
        lit_data[a[0]][0].update(OrderedDict.fromkeys(np.array(a[2].split(";"),dtype=int)+int(a[1])))
    if len(a[3]):
        lit_data[a[0]][1].update(OrderedDict.fromkeys(np.array(a[3].split(";"),dtype=int)+int(a[1])))

lit_sel = np.zeros(clengths[-1],dtype=bool)
for a in lit_data:
    lit_sel[get_residue(a)] = True

starts = np.array([np.where(ids==a)[0][0] for a in lit_data],dtype=int)
pos_indices = np.concatenate([list(lit_data[a][0])+starts[i] for i,a in enumerate(lit_data)])
pos_indices = np.array(list(OrderedDict.fromkeys(list(np.concatenate([pos_indices + k for k in range(-pospm,pospm+1)]))).keys()),dtype=int)
neg_indices = np.concatenate([list(lit_data[a][1])+starts[i] for i,a in enumerate(lit_data)]).astype(int)


#LOAD DB LABELS AND OTHER VARIABLES

if regenerate:
    db_annots = OrderedDict()
    rsa_raw = OrderedDict()
    conservation_raw = OrderedDict()
    for curid,pth in zip(ids,computed):
        with open(pth,"rb") as pfile:
            seqquery = pickle.load(pfile)
            scores = seqquery.annotations['scores']
            db_annots[curid] = scores[:,-1]
            rsa_raw[curid] = seqquery.letter_annotations['relative_solvent_accessibility_predicted']
            rsa_raw[curid] = np.hstack([np.sqrt(rsa_raw[curid][1:] * rsa_raw[curid][:-1]), rsa_raw[curid][-1]]).reshape(-1,1)
            conservation_raw[curid] = scores[:,6]

    newannots = np.vstack([db_annots[curid] for curid in ids])
    rsa_raw = np.vstack(rsa_raw.values())
    conservation = np.vstack(conservation)
    with open("annots.p","wb") as afile:
        pickle.dump(newannots, afile);

    with open("rsa_raw.p","wb") as afile:
        pickle.dump(rsa_raw, afile)

    with open("conservation.p","wb") as afile:
        pickle.dump(conservation, afile)

else:
    with open("Precomputed/annots.p","rb") as afile:
        newannots = pickle.load(afile)

    with open("Precomputed/rsa_raw.p","rb") as afile:
        rsa_raw = pickle.load(afile)

    with open("Precomputed/conservation.p","rb") as afile:
        conservation = pickle.load(afile)
    


#################################
#SET UP TRAINING AND TEST SETS
test_size = 0.2

pos_split = 0.8
random_state=0

truth = copy(newannots).reshape(1,-1)[0]
truth[np.isnan(truth)] = -1
truth = truth.astype(int)
test_negs = resample(np.where(truth==0)[0],random_state=random_state)[:np.floor(np.sum(truth==0)*test_size).astype(int)]
truth[test_negs] = -1
#divide positives into training and test sets
pos_sel = np.zeros(pos_indices.shape[0],dtype=bool)
pos_sel[np.arange(np.floor(pos_indices.shape[0]*pos_split).astype(int))] = True
pos_sel = resample(pos_sel,random_state=random_state)
truth[pos_indices[pos_sel]] = 1


fulltruth = copy(newannots).reshape(1,-1)[0]
fulltruth[np.isnan(fulltruth)] = -1
fulltruth = fulltruth.astype(int)
fulltruth[neg_indices] = 0
fulltruth[pos_indices] = 1


finaltest = np.array(list(set(np.concatenate([neg_indices,pos_indices[~pos_sel],test_negs]).tolist())),dtype=int)
finaltruth = fulltruth[finaltest]
finaltest_bool = np.zeros(features.shape[0],dtype=bool)
finaltest_bool[finaltest] = True


annot_indices = np.where(truth==0)[0]
#divide training data into training and validation sets. validation sets have 80% subsets of training positives
fold = 10
skf = sklearn.model_selection.StratifiedKFold(n_splits=fold,random_state=random_state)
cv = list(skf.split(features[truth!=-1,:1],truth[truth!=-1]))


#################################
#SCORING FUNCTIONS

#######SET NUMBER OF THREADS HERE
n_jobs = 8
verbosity = 10
n_estimators = 2000


res_per_prot=float(features.shape[0])/float(lengths.size)
pos_thres=3
fp_thres=1

def precision_recall_balanced_curve(estimator,X,y):
    desc_func = estimator.decision_function(X) if hasattr(estimator,'decision_function') else estimator.predict_proba(X)[:,1].reshape(-1,1) if hasattr(estimator,'predict_proba') else X
    adjust = 1/10
    weights = y.shape[0] / (2*np.bincount(y.astype(int))) * adjust
    sample_weight = weights[y.astype(int)]
    return sklearn.metrics.precision_recall_curve(y,desc_func,sample_weight=sample_weight)

def precision_recall_balanced_threshold(estimator,X,y,res_per_prot=res_per_prot,pos_thres=pos_thres,fp_thres=fp_thres):
    precision,recall,prthres = precision_recall_balanced_curve(estimator,X,y)
    return prthres[np.where((precision >= res_per_prot/pos_thres*recall*np.sum(y==1)/y.shape[0]/precision-float(fp_thres)/float(pos_thres))[:-1])[0][0]]

def precision_recall_balanced(estimator,X,y,reverse=False):
    precision,recall,prthres = precision_recall_balanced_curve(estimator,X,y)
    score = sklearn.metrics.auc(recall[::-1],precision[::-1])
    return 1-score if reverse else score


#################################
#CROSS-VALIDATION

def cv_train_rfc(X,y,cv,scoring,ttype="rfc"):
    if __name__ == "__main__":
        mult_range = [None]
        min_leaf_range = np.arange(1,5)
        max_features = [X.shape[1]]
        max_depth_range = [None]+list(range(1,7))
        param_grid = OrderedDict(max_features=max_features,class_weight=mult_range,min_samples_leaf=min_leaf_range,max_depth=max_depth_range)
        if ttype == "rfc":
            grid = GridSearchCV(forest.RandomForestClassifier(balanced=True,n_estimators=n_estimators,random_state=random_state,verbose=0), param_grid=param_grid, cv=cv, scoring=scoring, verbose=verbosity, n_jobs=n_jobs)
            #grid = GridSearchCV(forest.RandomForestClassifier(balanced=True,n_estimators=n_estimators,random_state=random_state,verbose=0), param_grid=param_grid, cv=cv, scoring=scoring, verbose=verbosity, n_jobs=n_jobs)
        else:
            grid = GridSearchCV(DecisionTreeClassifier(class_weight="balanced",random_state=random_state), param_grid=param_grid, cv=cv, scoring=scoring, verbose=verbosity, n_jobs=n_jobs)
        grid.fit(X,y)
        return grid



#################################
#TRAINED MODELS

#BRF
grid = cv_train_rfc(features[truth!=-1,:4],truth[truth!=-1],scoring=precision_recall_balanced,cv=cv)

#BRF (pHMM)
features_alt = np.array([0,4,2,3],dtype=int)
grid_phmm = cv_train_rfc(features[truth!=-1,features_alt], truth[truth!=-1], scoring=precision_recall_balanced,cv=cv)

#DT
grid_tree = cv_train_rfc(features[truth!=-1,:4],truth[truth!=-1],scoring=precision_recall_balanced,cv=cv,ttype="dt")

#DT (pHMM)
grid_tree_phmm = cv_train_rfc(features[truth!=-1,features_alt], truth[truth!=-1], scoring=precision_recall_balanced,cv=cv,ttype="dt")


#################################
#EVALUATION

#BEST PARAMS
best_param = [a for a in grid.grid_scores_ if a.parameters  == grid.best_params_][0]
best_param_phmm = [a for a in grid_phmm.grid_scores_ if a.parameters  == grid_phmm.best_params_][0]
best_param_dtc = [a for a in grid_dtc.grid_scores_ if a.parameters  == grid_dtc.best_params_][0]
best_param_dtc_phmm = [a for a in grid_dtc_phmm.grid_scores_ if a.parameters  == grid_phmm.best_params_][0]

#predictors
brf = grid.best_estimator_
brf_phmm = grid_phmm.best_esimator_
dtc = grid_tree.best_estimator_
dtc_phmm = grid_tree_phmm.best_estimator_


with open("Classifier.p","wb") as cfile:
    pickle.dump((brf,time.strftime("%Y-%m-%d")),cfile)

#################################
#PICKLE EVERYTHING
with open("results.p","wb") as dfile:
    pickle.dump({\
        "brf":brf,
        "brf_phmm":brf_phmm,
        "dtc":dtc,
        "dtc_phmm":dtc_phmm,
        "grid_scores":grid.grid_scores_,
        "grid_scores_phmm":grid_phmm.grid_scores_,
        "grid_scores_dtc":grid_tree.grid_scores_,
        "grid_scores_dtc_phmm":grid_tree_phmm.grid_scores_,
        "best_param":best_param,
        "best_param_phmm":best_param_phmm,
        "best_param_dtc":best_param_dtc,
        "best_param_dtc_phmm":best_param_dtc_phmm,
        "fulltruth":fulltruth,
        "finaltruth":finaltruth,
        "finaltest":finaltest,
        "truth":truth,
        "pos_sel":pos_sel,
        "pos_indices":pos_indices,
        "neg_indices":neg_indices,
        "annot_indices":annot_indices,
        "splits":splits,
        "cv":cv,
        "possplits":possplits,
        "lit_data":lit_data,
        "lit_sel":lit_sel,
        "raw_data":raw_data,
        "time": time.strftime("%Y-%m-%d"),
    },dfile)

#################################
#FITTED PARAMETERS

tagcutoff_default = precision_recall_balanced_threshold(brf, features[truth!=-1,:4],truth[truth!=-1])

#secondary structure decay coefficient
ss_distance_decay = 2

#residue null model, taken from hhblits
res_probs_log_raw = np.array([-3.706, -5.728, -4.211, -4.064, -4.839, -3.729, -4.763, -4.308, -4.069, -3.323, -5.509, -4.64 , -4.464, -4.937, -4.285, -4.423, -3.815, -3.783, -6.325, -4.665]) * np.log(2)
res_probs_log = res_probs_log_raw - scipy.misc.logsumexp(res_probs_log_raw)
base_log_prob = -99.999 * np.log(2)


rsa_fit = scipy.stats.beta.fit(rsa_raw)
seqvar_fit = scipy.stats.beta.fit(conservation)

fitparams = {\
    'rsa_fit': rsa_fit,
    'seqvar_fit': seqvar_fit,
    'ss_distance_decay':ss_distance_decay,
    'res_probs_log_raw':res_probs_log_raw,
    'res_probs_log':res_probs_log,
    'base_log_prob':base_log_prob,
    'tagcutoff_default':tagcutoff_default,
}

with open("FittedParameters.p", "wb") as pfile:
    pickle.dump(fitparams, pfile)


