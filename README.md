Info
=====
GapMiner is a tool for predicting internal protein tagging sites (see publication [[1]] for details). The results are viewable using the GapMiner Viewer hosted at the GapMinerDB repository [[2]].

License
-------
GapMiner is provided under the [Mozilla Public License 2.0](LICENSE).

### Dependencies
- Python >= 3.5, under the [PSF License](https://docs.python.org/3/license.html)
- biopython, under the [BSD 3-Clause License](https://github.com/biopython/biopython/blob/master/LICENSE.rst)
- lxml, under the [BSD 3-Clause License](https://github.com/lxml/lxml/blob/master/LICENSES.txt)
- numpy, under the [BSD 3-Clause License](https://docs.scipy.org/doc/numpy-1.10.0/license.html)
- requests, under the [Apache 2.0 License](http://docs.python-requests.org/en/master/user/intro/)
- beautifulsoup4, under the [MIT License](https://www.crummy.com/software/BeautifulSoup/)
- sklearn, under the [BSD 3-Clause License](https://github.com/scikit-learn/scikit-learn/blob/master/COPYING)
- scipy, under the [BSD 3-Clause License](https://docs.scipy.org/doc/numpy-1.10.0/license.html)

External Dependencies
---------------------
- [NetSurfP 1.0d](http://www.cbs.dtu.dk/cgi-bin/sw_request?netsurfp+1.0)
- [BLAST 2.2.18](https://ftp.ncbi.nih.gov/blast/executables/legacy/2.2.18/)
- [hh-suite 2.0.16](http://wwwuser.gwdg.de/%7Ecompbiol/data/hhsuite/releases/all/)
- (optionally:) [Clustal Omega](http://www.clustal.org/omega/) and [Delta-BLAST](https://ftp.ncbi.nih.gov/blast/executables/blast+/)

Installation
------------
Installation was tested on Ubuntu 16.04 as well as 18.04 and will take a few hours. 
The assumption is that you are starting these steps in /home/gapminer/Desktop (as in the VM we provide), if you use a different path you will need to adapt some configuration files (see below).
Start by installing Git and cloning this repository:
    
    sudo apt install -y git
    git clone https://gitlab.com/bpl.ethz/GapMiner.git
    cd GapMiner/GapMiner

Then run

    ./gapminer-install.sh
    
This script will require you to download NetsurfP 1.0d from http://www.cbs.dtu.dk/cgi-bin/sw_request?netsurfp+1.0 and put the archive into the GapMiner folder (this is due to licensing restrictions).
    
Finally, adapt the paths in [config.py](GapMiner/config.py), and, if you did not install into /home/gapminer/Desktop/ in netsurfp. Then, e.g., run gapminer on PheS by running

    source gapminer-startup.sh
    ./gapminer -c config.py P08312
    
The resulting JSON file can be then be viewed with the GapMiner viewer (http://www.bpl.ethz.ch/software/gapminer/, source code git repository at https://gitlab.com/bpl.ethz/GapMinerDB):
![Loading JSON result files with the GapMiner viewer](SupplementaryMethods/Figures/Load-json-output.png "Loading JSON result files with the GapMiner viewer")

Authors
-------

    Sabine Österle <sabine.oesterle@bsse.ethz.ch>
    Lukas Widmer <lukas.widmer@bsse.ethz.ch>
    Harun Mustafa <harun.mustafa@inf.ethz.ch>
    Niresh Berinpanathan <nireshb@student.ethz.ch>
    Tania Michelle Roberts <tania.roberts@bsse.ethz.ch>
    Jörg Stelling <joerg.stelling@bsse.ethz.ch>
    Sven Panke <sven.panke@bsse.ethz.ch>
    
 References
==========
[1]: http://google.com
1. S. Österle†, Lukas A. Widmer†, Harun Mustafa†, N. Berinpanathan, T. M. Roberts, J. Stelling and S. Panke (2017)  
GapMiner – Prediction of Internal Protein Tagging Sites  
† equally contributed  
[To be submitted.][1]
[2]: https://gitlab.com/bpl.ethz/GapMinerDB
2. [GapMinerDB Repository][2]
